# Calendrier républicain

Le calendrier républicain est une application <i>web</i> permettant de consulter la date du jour en calendrier républicain, de parcourir ce calendrier et de convertir des dates de et vers le calendrier grégorien.

## Installation

Ce projet repose sur un environnement <cite>Apache HTTP Server</cite> et <cite>PHP</cite>.

## Support

Pas de maintenance prévue pour le moment.

## Feuille de route

* Refonte complète en application <cite>web</cite> moderne
    * Application client en <cite>Vue.js</cite>
    * Éventuellement avec un <cite>backend</cite> fournissant l’API, solution à l’étude mais non privilégiée à ce jour faute de besoin réel

## Contribuer

Aucune contribution acceptée pour le moment.

## Auteurs et contributeurs

Principalement écrit par Florian Monfort.

## License

Cette application est sous licence GPLv3.
