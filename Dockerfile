FROM php:8.2-apache
RUN a2enmod rewrite
WORKDIR /var/www/html
COPY src/ .
RUN chown -R www-data:www-data .
EXPOSE 80
CMD ["apache2-foreground"]
