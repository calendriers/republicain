<?php

	// Grégorien

	$f3->set("MILLISECONDES_PAR_SECONDE", 1_000);

	$f3->set("SECONDES_PAR_MINUTE", 60);

	$f3->set("MINUTES_PAR_HEURE", 60);
	$f3->set("SECONDES_PAR_HEURE", $f3->get("SECONDES_PAR_MINUTE") * $f3->get("MINUTES_PAR_HEURE"));

	$f3->set("HEURES_PAR_JOUR", 24);
	$f3->set("MINUTES_PAR_JOUR", $f3->get("MINUTES_PAR_HEURE") * $f3->get("HEURES_PAR_JOUR"));
	$f3->set("SECONDES_PAR_JOUR", $f3->get("SECONDES_PAR_HEURE") * $f3->get("HEURES_PAR_JOUR"));

	$f3->set("JOURS_PAR_SEMAINE", 7);
	$f3->set("HEURES_PAR_SEMAINE", $f3->get("HEURES_PAR_JOUR") * $f3->get("JOURS_PAR_SEMAINE"));
	$f3->set("MINUTES_PAR_SEMAINE", $f3->get("MINUTES_PAR_JOUR") * $f3->get("JOURS_PAR_SEMAINE"));
	$f3->set("SECONDES_PAR_SEMAINE", $f3->get("SECONDES_PAR_JOUR") * $f3->get("JOURS_PAR_SEMAINE"));

	$f3->set("MOIS_PAR_AN", 12);

	$f3->set("SEMAINES_PAR_AN", 52);
	$f3->set("JOURS_PAR_AN", $f3->get("JOURS_PAR_SEMAINE") * $f3->get("SEMAINES_PAR_AN") + 1);
	$f3->set("JOURS_PAR_AN_BISSEXTIL", $f3->get("JOURS_PAR_AN") + 1);
	$f3->set("HEURES_PAR_AN", $f3->get("HEURES_PAR_JOUR") * $f3->get("JOURS_PAR_AN"));
	$f3->set("MINUTES_PAR_AN", $f3->get("MINUTES_PAR_JOUR") * $f3->get("JOURS_PAR_AN"));
	$f3->set("SECONDES_PAR_AN", $f3->get("SECONDES_PAR_JOUR") * $f3->get("JOURS_PAR_AN"));

	// Républicain

	$f3->set("ANNÉE_COMMENCEMENT", 1792);
	$f3->set("JOUR_COMMENCEMENT", 265); // 22 septembre d’une année non bissextile

	$f3->set("JOURS_PAR_DÉCADE", 10);
	$f3->set("DÉCADES_PAR_MOIS", 3);

	$f3->set("CENTISECONDES_PAR_SECONDE_RÉPUBLICAINE", 100);

	$f3->set("SECONDES_PAR_MINUTE_RÉPUBLICAINE", 100);
	$f3->set("CENTISECONDES_PAR_MINUTE_RÉPUBLICAINE", $f3->get("CENTISECONDES_PAR_SECONDE_RÉPUBLICAINE") * $f3->get("SECONDES_PAR_MINUTE_RÉPUBLICAINE"));

	$f3->set("MINUTES_PAR_HEURE_RÉPUBLICAINE", 100);
	$f3->set("SECONDES_PAR_HEURE_RÉPUBLICAINE", $f3->get("SECONDES_PAR_MINUTE_RÉPUBLICAINE") * $f3->get("MINUTES_PAR_HEURE_RÉPUBLICAINE"));
	$f3->set("CENTISECONDES_PAR_HEURE_RÉPUBLICAINE", $f3->get("CENTISECONDES_PAR_SECONDE_RÉPUBLICAINE") * $f3->get("SECONDES_PAR_HEURE_RÉPUBLICAINE"));

	$f3->set("HEURES_PAR_JOUR_RÉPUBLICAIN", 10);
	$f3->set("MINUTES_PAR_JOUR_RÉPUBLICAIN", $f3->get("MINUTES_PAR_HEURE_RÉPUBLICAINE") * $f3->get("HEURES_PAR_JOUR_RÉPUBLICAIN"));
	$f3->set("SECONDES_PAR_JOUR_RÉPUBLICAIN", $f3->get("SECONDES_PAR_HEURE_RÉPUBLICAINE") * $f3->get("HEURES_PAR_JOUR_RÉPUBLICAIN"));
	$f3->set("CENTISECONDES_PAR_JOUR_RÉPUBLICAIN", $f3->get("CENTISECONDES_PAR_SECONDE_RÉPUBLICAINE") * $f3->get("SECONDES_PAR_JOUR_RÉPUBLICAIN"));

	$f3->set("JOURS_PAR_MOIS_RÉPUBLICAIN", $f3->get("JOURS_PAR_DÉCADE") * $f3->get("DÉCADES_PAR_MOIS"));
	$f3->set("HEURES_PAR_MOIS_RÉPUBLICAIN", $f3->get("HEURES_PAR_JOUR_RÉPUBLICAIN") * $f3->get("JOURS_PAR_MOIS_RÉPUBLICAIN"));
	$f3->set("MINUTES_PAR_MOIS_RÉPUBLICAIN", $f3->get("MINUTES_PAR_JOUR_RÉPUBLICAIN") * $f3->get("JOURS_PAR_MOIS_RÉPUBLICAIN"));
	$f3->set("SECONDES_PAR_MOIS_RÉPUBLICAIN", $f3->get("SECONDES_PAR_JOUR_RÉPUBLICAIN") * $f3->get("JOURS_PAR_MOIS_RÉPUBLICAIN"));
	$f3->set("CENTISECONDES_PAR_MOIS_RÉPUBLICAIN", $f3->get("CENTISECONDES_PAR_SECONDE_RÉPUBLICAINE") * $f3->get("SECONDES_PAR_MOIS_RÉPUBLICAIN"));

	$f3->set("DÉCADES_PAR_AN", $f3->get("DÉCADES_PAR_MOIS") * $f3->get("MOIS_PAR_AN") + 1); // Semi‐décade complémentaire des Sans‐culottides
	$f3->set("JOURS_PAR_AN_RÉPUBLICAIN", $f3->get("JOURS_PAR_DÉCADE") * $f3->get("DÉCADES_PAR_AN"));
	$f3->set("JOURS_PAR_AN_RÉPUBLICAIN_SEXTIL", $f3->get("JOURS_PAR_AN_RÉPUBLICAIN") + 1);
	$f3->set("HEURES_PAR_AN_RÉPUBLICAIN", $f3->get("HEURES_PAR_JOUR_RÉPUBLICAIN") * $f3->get("JOURS_PAR_AN_RÉPUBLICAIN"));
	$f3->set("MINUTES_PAR_AN_RÉPUBLICAIN", $f3->get("MINUTES_PAR_JOUR_RÉPUBLICAIN") * $f3->get("JOURS_PAR_AN_RÉPUBLICAIN"));
	$f3->set("SECONDES_PAR_AN_RÉPUBLICAIN", $f3->get("SECONDES_PAR_JOUR_RÉPUBLICAIN") * $f3->get("JOURS_PAR_AN_RÉPUBLICAIN"));
	$f3->set("CENTISECONDES_PAR_MOIS_RÉPUBLICAIN", $f3->get("CENTISECONDES_PAR_SECONDE_RÉPUBLICAINE") * $f3->get("SECONDES_PAR_AN_RÉPUBLICAIN"));

	$f3->set("JOUR_COMMENCEMENT_SANS_CULOTTIDES", $f3->get("JOURS_PAR_MOIS_RÉPUBLICAIN") * $f3->get("MOIS_PAR_AN"));

	// Pour les jetons de session

	// Un jeton dure quatre heures
	$f3->set("DURÉE_LIMITE_JETON", 4 * $f3->get("SECONDES_PAR_HEURE"));

	// Pour les paramètres

	// Un témoin dure un an
	$f3->set("DURÉE_TÉMOIN", $f3->get("SECONDES_PAR_AN"));

	// Les valeurs des paramètres
	$f3->set("CALENDRIER_HISTORIQUE", "historique");
	$f3->set("CALENDRIER_RÉVISÉ", "révisé");

	$f3->set("ORTHOGRAPHE_CLASSIQUE", "classique");
	$f3->set("ORTHOGRAPHE_TRADITIONNELLE", "traditionnelle");
	$f3->set("ORTHOGRAPHE_MODERNE", "moderne");

	// Fuseaux horaires enregistrés par l’IANA (version 2022f)
	// Rester à jour : https://data.iana.org/time-zones/tzdb/NEWS
	$f3->set("PAIRES_FUSEAUX_HORAIRES", [
		"Africa/Abidjan" => "Abidjan",
		"Africa/Accra" => "Accra",
		"Africa/Addis_Ababa" => "Addis‐Abeba",
		"Africa/Algiers" => "Alger",
		"Africa/Asmara" => "Asmara",
		"Africa/Bamako" => "Bamako",
		"Africa/Bangui" => "Bangui",
		"Africa/Banjul" => "Banjul",
		"Africa/Bissau" => "Bissau",
		"Africa/Blantyre" => "Blantyre",
		"Africa/Brazzaville" => "Brazzaville",
		"Africa/Bujumbura" => "Bujumbura",
		"Africa/Cairo" => "Le Caire",
		"Africa/Casablanca" => "Casablanca",
		"Africa/Ceuta" => "Ceuta",
		"Africa/Conakry" => "Conakry",
		"Africa/Dakar" => "Dakar",
		"Africa/Dar_es_Salaam" => "Dar es Salam",
		"Africa/Djibouti" => "Djibouti",
		"Africa/Douala" => "Douala",
		"Africa/El_Aaiun" => "Laâyoune",
		"Africa/Freetown" => "Freetown",
		"Africa/Gaborone" => "Gaborone",
		"Africa/Harare" => "Hararé",
		"Africa/Johannesburg" => "Johannesbourg",
		"Africa/Juba" => "Djuba",
		"Africa/Kampala" => "Kampala",
		"Africa/Khartoum" => "Khartoum",
		"Africa/Kigali" => "Kigali",
		"Africa/Kinshasa" => "Kinshasa",
		"Africa/Lagos" => "Lagos",
		"Africa/Libreville" => "Libreville",
		"Africa/Lome" => "Lomé",
		"Africa/Luanda" => "Luanda",
		"Africa/Lubumbashi" => "Lubumbashi",
		"Africa/Lusaka" => "Lusaka",
		"Africa/Malabo" => "Malabo",
		"Africa/Maputo" => "Maputo",
		"Africa/Maseru" => "Maseru",
		"Africa/Mbabane" => "Mbabané",
		"Africa/Mogadishu" => "Mogadiscio",
		"Africa/Monrovia" => "Monrovia",
		"Africa/Nairobi" => "Nairobi",
		"Africa/Ndjamena" => "N’Djaména",
		"Africa/Niamey" => "Niamey",
		"Africa/Nouakchott" => "Nouakchott",
		"Africa/Ouagadougou" => "Ouagadougou",
		"Africa/Porto-Novo" => "Porto‐Novo",
		"Africa/Sao_Tome" => "Sao Tomé",
		"Africa/Tripoli" => "Tripoli",
		"Africa/Tunis" => "Tunis",
		"Africa/Windhoek" => "Windhoek",

		"America/Adak" => "Adak",
		"America/Anchorage" => "Anchorage",
		"America/Anguilla" => "Anguilla",
		"America/Antigua" => "Antigua",
		"America/Araguaina" => "Araguaína",
		"America/Argentina/Buenos_Aires" => "Buenos Aires",
		"America/Argentina/Catamarca" => "San Fernando del Valle de Catamarca",
		"America/Argentina/Cordoba" => "Córdoba",
		"America/Argentina/Jujuy" => "San Salvador de Jujuy",
		"America/Argentina/La_Rioja" => "La Rioja",
		"America/Argentina/Mendoza" => "Mendoza",
		"America/Argentina/Rio_Gallegos" => "Rio Gallegos",
		"America/Argentina/Salta" => "Salta",
		"America/Argentina/San_Juan" => "San Juan",
		"America/Argentina/San_Luis" => "San Luis",
		"America/Argentina/Tucuman" => "San Miguel de Tucumán",
		"America/Argentina/Ushuaia" => "Ushuaïa",
		"America/Aruba" => "Aruba",
		"America/Asuncion" => "Asuncion",
		"America/Atikokan" => "Atikokan",
		"America/Bahia" => "Bahia",
		"America/Bahia_Banderas" => "Bahía de Banderas",
		"America/Barbados" => "La Barbade",
		"America/Belem" => "Belém",
		"America/Belize" => "Le Bélize",
		"America/Blanc-Sablon" => "Blanc‐Sablon",
		"America/Boa_Vista" => "Boa Vista",
		"America/Bogota" => "Bogota",
		"America/Boise" => "Boise",
		"America/Cambridge_Bay" => "Cambridge Bay",
		"America/Campo_Grande" => "Campo Grande",
		"America/Cancun" => "Cancún",
		"America/Caracas" => "Santiago de León de Caracas",
		"America/Cayenne" => "Cayenne",
		"America/Cayman" => "Iles Caïmans",
		"America/Chicago" => "Chicago",
		"America/Chihuahua" => "Chihuahua",
		"America/Costa_Rica" => "Costa Rica",
		"America/Creston" => "Creston",
		"America/Cuiaba" => "Cuiabá",
		"America/Curacao" => "Curaçao",
		"America/Danmarkshavn" => "Danmarkshavn",
		"America/Dawson" => "Dawson",
		"America/Dawson_Creek" => "Dawson Creek",
		"America/Denver" => "Denver",
		"America/Detroit" => "Détroit",
		"America/Dominica" => "La Dominique",
		"America/Edmonton" => "Edmonton",
		"America/Eirunepe" => "Eirunepé",
		"America/El_Salvador" => "Le Salvador",
		"America/Fort_Nelson" => "Fort Nelson",
		"America/Fortaleza" => "Fortaleza",
		"America/Glace_Bay" => "Glace Bay",
		"America/Goose_Bay" => "Goose Bay",
		"America/Grand_Turk" => "Grand Turk",
		"America/Grenada" => "La Grenade",
		"America/Guadeloupe" => "La Guadeloupe",
		"America/Guatemala" => "Guatémala",
		"America/Guayaquil" => "Guayaquil",
		"America/Guyana" => "Le Guyana",
		"America/Halifax" => "Halifax",
		"America/Havana" => "La Havane",
		"America/Hermosillo" => "Hermosillo",
		"America/Indiana/Indianapolis" => "Indianapolis",
		"America/Indiana/Knox" => "Knox",
		"America/Indiana/Marengo" => "Marengo",
		"America/Indiana/Petersburg" => "Petersburg",
		"America/Indiana/Tell_City" => "Tell City",
		"America/Indiana/Vevay" => "Vevay",
		"America/Indiana/Vincennes" => "Vincennes",
		"America/Indiana/Winamac" => "Winamac",
		"America/Inuvik" => "Inuvik",
		"America/Iqaluit" => "Iqaluit",
		"America/Jamaica" => "La Jamaïque",
		"America/Juneau" => "Juneau",
		"America/Kentucky/Louisville" => "Louisville",
		"America/Kentucky/Monticello" => "Monticello",
		"America/Kralendijk" => "Kralendijk",
		"America/La_Paz" => "La Paz",
		"America/Lima" => "Lima",
		"America/Los_Angeles" => "Los Angeles",
		"America/Lower_Princes" => "Lower Princes",
		"America/Maceio" => "Maceió",
		"America/Managua" => "Managua",
		"America/Manaus" => "Manaus",
		"America/Marigot" => "Marigot",
		"America/Martinique" => "La Martinique",
		"America/Matamoros" => "Matamoros",
		"America/Mazatlan" => "Mazatlan",
		"America/Menominee" => "Menominee",
		"America/Merida" => "Mérida",
		"America/Metlakatla" => "Metlakatla",
		"America/Mexico_City" => "Mexico",
		"America/Miquelon" => "Miquelon",
		"America/Moncton" => "Moncton",
		"America/Monterrey" => "Monterrey",
		"America/Montevideo" => "Montévidéo",
		"America/Montserrat" => "Montserrat",
		"America/Nassau" => "Nassau",
		"America/New_York" => "New York",
		"America/Nipigon" => "Nipigon",
		"America/Nome" => "Nome",
		"America/Noronha" => "Fernando de Noronha",
		"America/North_Dakota/Beulah" => "Beulah",
		"America/North_Dakota/Center" => "Center",
		"America/North_Dakota/New_Salem" => "New Salem",
		"America/Nuuk" => "Nuuk",
		"America/Ojinaga" => "Ojinaga",
		"America/Panama" => "Panama",
		"America/Pangnirtung" => "Pangniqtuuq",
		"America/Paramaribo" => "Paramaribo",
		"America/Phoenix" => "Phoenix",
		"America/Port-au-Prince" => "Port‐au‐Prince",
		"America/Port_of_Spain" => "Port‐d’Espagne",
		"America/Porto_Velho" => "Porto Velho",
		"America/Puerto_Rico" => "Porto Rico",
		"America/Punta_Arenas" => "Punta Arenas",
		"America/Rainy_River" => "Rainy River",
		"America/Rankin_Inlet" => "Rankin Inlet",
		"America/Recife" => "Récife",
		"America/Regina" => "Regina",
		"America/Resolute" => "Resolute",
		"America/Rio_Branco" => "Rio Branco",
		"America/Santarem" => "Santarém",
		"America/Santiago" => "Santiago",
		"America/Santo_Domingo" => "Saint‐Domingue",
		"America/Sao_Paulo" => "Sao Paulo",
		"America/Scoresbysund" => "Ittoqqortoormiit",
		"America/Sitka" => "Sitka",
		"America/St_Barthelemy" => "Saint‐Barthélemy",
		"America/St_Johns" => "Saint John’s",
		"America/St_Kitts" => "Saint‐Christophe",
		"America/St_Lucia" => "Sainte‐Lucie",
		"America/St_Thomas" => "Saint‐Thomas",
		"America/St_Vincent" => "Saint‐Vincent",
		"America/Swift_Current" => "Swift Current",
		"America/Tegucigalpa" => "Tégucigalpa",
		"America/Thule" => "Ile Thule",
		"America/Thunder_Bay" => "Thunder Bay",
		"America/Tijuana" => "Tijuana",
		"America/Toronto" => "Toronto",
		"America/Tortola" => "Tortola",
		"America/Vancouver" => "Vancouver",
		"America/Whitehorse" => "Whitehorse",
		"America/Winnipeg" => "Winnipeg",
		"America/Yakutat" => "Yakutat",
		"America/Yellowknife" => "Yellowknife",

		"Antarctica/Casey" => "Casey",
		"Antarctica/Davis" => "Davis",
		"Antarctica/DumontDUrville" => "Dumont‐d’Urville",
		"Antarctica/Macquarie" => "Ile Macquarie",
		"Antarctica/Mawson" => "Mawson",
		"Antarctica/McMurdo" => "McMurdo",
		"Antarctica/Palmer" => "Palmer",
		"Antarctica/Rothera" => "Rothera",
		"Antarctica/Syowa" => "Shōwa",
		"Antarctica/Troll" => "Troll",
		"Antarctica/Vostok" => "Vostok",

		"Arctic/Longyearbyen" => "Longyearbyen",

		"Asia/Aden" => "Aden",
		"Asia/Almaty" => "Almaty",
		"Asia/Amman" => "Amman",
		"Asia/Anadyr" => "Anadyr",
		"Asia/Aqtau" => "Aktaou",
		"Asia/Aqtobe" => "Aktioubé",
		"Asia/Ashgabat" => "Achgabat",
		"Asia/Atyrau" => "Atyraou",
		"Asia/Baghdad" => "Bagdad",
		"Asia/Bahrain" => "Bahreïn",
		"Asia/Baku" => "Bakou",
		"Asia/Bangkok" => "Bangkok",
		"Asia/Barnaul" => "Barnaoul",
		"Asia/Beirut" => "Beyrouth",
		"Asia/Bishkek" => "Bichkek",
		"Asia/Brunei" => "Le Brunéi",
		"Asia/Chita" => "Chita",
		"Asia/Choibalsan" => "Choybalsan",
		"Asia/Colombo" => "Colombo",
		"Asia/Damascus" => "Damas",
		"Asia/Dhaka" => "Dacca",
		"Asia/Dili" => "Dili",
		"Asia/Dubai" => "Doubaï",
		"Asia/Dushanbe" => "Douchanbé",
		"Asia/Famagusta" => "Famagouste",
		"Asia/Gaza" => "Gaza",
		"Asia/Hebron" => "Hébron",
		"Asia/Ho_Chi_Minh" => "Hô Chi Minh",
		"Asia/Hong_Kong" => "Hong Kong",
		"Asia/Hovd" => "Khovd",
		"Asia/Irkutsk" => "Irkoutsk",
		"Asia/Jakarta" => "Jakarta",
		"Asia/Jayapura" => "Jayapura",
		"Asia/Jerusalem" => "Jérusalem",
		"Asia/Kabul" => "Kaboul",
		"Asia/Kamchatka" => "Kamtchatka",
		"Asia/Karachi" => "Karachi",
		"Asia/Kathmandu" => "Katmandou",
		"Asia/Khandyga" => "Khandyga",
		"Asia/Kolkata" => "Calcutta",
		"Asia/Krasnoyarsk" => "Krasnoïarsk",
		"Asia/Kuala_Lumpur" => "Kuala Lumpur",
		"Asia/Kuching" => "Kuching",
		"Asia/Kuwait" => "Koweït",
		"Asia/Macau" => "Macao",
		"Asia/Magadan" => "Magadane",
		"Asia/Makassar" => "Makassar",
		"Asia/Manila" => "Manille",
		"Asia/Muscat" => "Mascate",
		"Asia/Nicosia" => "Nicosie",
		"Asia/Novokuznetsk" => "Novokouznetsk",
		"Asia/Novosibirsk" => "Novossibirsk",
		"Asia/Omsk" => "Omsk",
		"Asia/Oral" => "Oural",
		"Asia/Phnom_Penh" => "Phnom Penh",
		"Asia/Pontianak" => "Pontianak",
		"Asia/Pyongyang" => "Pyongyang",
		"Asia/Qatar" => "Le Qatar",
		"Asia/Qostanay" => "Kostanaï",
		"Asia/Qyzylorda" => "Kyzylorda",
		"Asia/Riyadh" => "Riyad",
		"Asia/Sakhalin" => "Sakhaline",
		"Asia/Samarkand" => "Samarcande",
		"Asia/Seoul" => "Séoul",
		"Asia/Shanghai" => "Shanghaï",
		"Asia/Singapore" => "Singapour",
		"Asia/Srednekolymsk" => "Srednékolymsk",
		"Asia/Taipei" => "Taipei",
		"Asia/Tashkent" => "Tachkent",
		"Asia/Tbilisi" => "Tbilissi",
		"Asia/Tehran" => "Téhéran",
		"Asia/Thimphu" => "Thimphou",
		"Asia/Tokyo" => "Tokyo",
		"Asia/Tomsk" => "Tomsk",
		"Asia/Ulaanbaatar" => "Oulan‐Bator",
		"Asia/Urumqi" => "Ürümqi",
		"Asia/Ust-Nera" => "Oust‐Néra",
		"Asia/Vientiane" => "Vientiane",
		"Asia/Vladivostok" => "Vladivostok",
		"Asia/Yakutsk" => "Iakoutsk",
		"Asia/Yangon" => "Rangoun",
		"Asia/Yekaterinburg" => "Iekaterinbourg",
		"Asia/Yerevan" => "Erevan",

		"Atlantic/Azores" => "Les Açores",
		"Atlantic/Bermuda" => "Les Bermudes",
		"Atlantic/Canary" => "Iles Canaries",
		"Atlantic/Cape_Verde" => "Le Cap‐Vert",
		"Atlantic/Faroe" => "Iles Féroé",
		"Atlantic/Madeira" => "Madère",
		"Atlantic/Reykjavik" => "Reykjavik",
		"Atlantic/South_Georgia" => "La Géorgie du Sud",
		"Atlantic/St_Helena" => "Sainte‐Hélène",
		"Atlantic/Stanley" => "Stanley",

		"Australia/Adelaide" => "Adélaïde",
		"Australia/Brisbane" => "Brisbane",
		"Australia/Broken_Hill" => "Broken Hill",
		"Australia/Darwin" => "Darwin",
		"Australia/Eucla" => "Eucla",
		"Australia/Hobart" => "Hobart",
		"Australia/Lindeman" => "Lindeman",
		"Australia/Lord_Howe" => "Lord Howe",
		"Australia/Melbourne" => "Melbourne",
		"Australia/Perth" => "Perth",
		"Australia/Sydney" => "Sydney",

		"Europe/Amsterdam" => "Amsterdam",
		"Europe/Andorra" => "Andorre",
		"Europe/Astrakhan" => "Astrakhan",
		"Europe/Athens" => "Athènes",
		"Europe/Belgrade" => "Belgrade",
		"Europe/Berlin" => "Berlin",
		"Europe/Bratislava" => "Bratislava",
		"Europe/Brussels" => "Bruxelles",
		"Europe/Bucharest" => "Bucarest",
		"Europe/Budapest" => "Budapest",
		"Europe/Busingen" => "Büsingen am Hochrhein",
		"Europe/Chisinau" => "Chisinau",
		"Europe/Copenhagen" => "Copenhague",
		"Europe/Dublin" => "Dublin",
		"Europe/Gibraltar" => "Gibraltar",
		"Europe/Guernsey" => "Guernesey",
		"Europe/Helsinki" => "Helsinki",
		"Europe/Isle_of_Man" => "Ile de Man",
		"Europe/Istanbul" => "Istamboul",
		"Europe/Jersey" => "Jersey",
		"Europe/Kaliningrad" => "Kaliningrad",
		"Europe/Kyiv" => "Kiev",
		"Europe/Kirov" => "Kirov",
		"Europe/Lisbon" => "Lisbonne",
		"Europe/Ljubljana" => "Ljubljana",
		"Europe/London" => "Londres",
		"Europe/Luxembourg" => "Luxembourg",
		"Europe/Madrid" => "Madrid",
		"Europe/Malta" => "Malte",
		"Europe/Mariehamn" => "Mariehamn",
		"Europe/Minsk" => "Minsk",
		"Europe/Monaco" => "Monaco",
		"Europe/Moscow" => "Moscou",
		"Europe/Oslo" => "Oslo",
		"Europe/Paris" => "Paris",
		"Europe/Podgorica" => "Podgorica",
		"Europe/Prague" => "Prague",
		"Europe/Riga" => "Riga",
		"Europe/Rome" => "Rome",
		"Europe/Samara" => "Samara",
		"Europe/San_Marino" => "Saint‐Marin",
		"Europe/Sarajevo" => "Sarajevo",
		"Europe/Saratov" => "Saratov",
		"Europe/Simferopol" => "Simferopol",
		"Europe/Skopje" => "Skopje",
		"Europe/Sofia" => "Sofia",
		"Europe/Stockholm" => "Stockholm",
		"Europe/Tallinn" => "Tallinn",
		"Europe/Tirane" => "Tirana",
		"Europe/Ulyanovsk" => "Oulianovsk",
		"Europe/Uzhgorod" => "Oujhorod",
		"Europe/Vaduz" => "Vaduz",
		"Europe/Vatican" => "Le Vatican",
		"Europe/Vienna" => "Vienne",
		"Europe/Vilnius" => "Vilnius",
		"Europe/Volgograd" => "Volgograd",
		"Europe/Warsaw" => "Varsovie",
		"Europe/Zagreb" => "Zagreb",
		"Europe/Zaporozhye" => "Zaporojie",
		"Europe/Zurich" => "Zurich",

		"Indian/Antananarivo" => "Antananarivo",
		"Indian/Chagos" => "Les Chagos",
		"Indian/Christmas" => "Ile Christmas",
		"Indian/Cocos" => "Iles Cocos",
		"Indian/Comoro" => "Les Comores",
		"Indian/Kerguelen" => "Iles Kerguelen",
		"Indian/Mahe" => "Mahé",
		"Indian/Maldives" => "Les Maldives",
		"Indian/Mauritius" => "Maurice",
		"Indian/Mayotte" => "Mayotte",
		"Indian/Reunion" => "La Réunion",

		"Pacific/Apia" => "Apia",
		"Pacific/Auckland" => "Auckland",
		"Pacific/Bougainville" => "Bougainville",
		"Pacific/Chatham" => "Chatham",
		"Pacific/Chuuk" => "Chuuk",
		"Pacific/Easter" => "Easter",
		"Pacific/Efate" => "Éfaté",
		"Pacific/Fakaofo" => "Fakaofo",
		"Pacific/Fiji" => "Iles Fidji",
		"Pacific/Funafuti" => "Funafuti",
		"Pacific/Galapagos" => "Les Galápagos",
		"Pacific/Gambier" => "Iles Gambier",
		"Pacific/Guadalcanal" => "Guadalcanal",
		"Pacific/Guam" => "Guam",
		"Pacific/Honolulu" => "Honolulu",
		"Pacific/Kanton" => "Canton",
		"Pacific/Kiritimati" => "Kiritimati",
		"Pacific/Kosrae" => "Kosrae",
		"Pacific/Kwajalein" => "Kwajalein",
		"Pacific/Majuro" => "Majuro",
		"Pacific/Marquesas" => "Iles Marquises",
		"Pacific/Midway" => "Iles Midway",
		"Pacific/Nauru" => "Nauru",
		"Pacific/Niue" => "Niué",
		"Pacific/Norfolk" => "Norfolk",
		"Pacific/Noumea" => "Nouméa",
		"Pacific/Pago_Pago" => "Pago Pago",
		"Pacific/Palau" => "Les Palaos",
		"Pacific/Pitcairn" => "Pitcairn",
		"Pacific/Pohnpei" => "Pohnpei",
		"Pacific/Port_Moresby" => "Port Moresby",
		"Pacific/Rarotonga" => "Rarotonga",
		"Pacific/Saipan" => "Saïpan",
		"Pacific/Tahiti" => "Tahiti",
		"Pacific/Tarawa" => "Tarawa",
		"Pacific/Tongatapu" => "Tongatapu",
		"Pacific/Wake" => "Wake",
		"Pacific/Wallis" => "Wallis",
	]);

	$f3->set("FUSEAUX_HORAIRES", array_keys($f3->get("PAIRES_FUSEAUX_HORAIRES")));
	$f3->set("NOMS_FUSEAUX_HORAIRES", array_values($f3->get("PAIRES_FUSEAUX_HORAIRES")));

	$f3->set("ORDINATION_TRADITIONNELLE", "traditionnelle");
	$f3->set("ORDINATION_MODERNE", "moderne");

	$f3->set("CHIFFRAGE_ARABE", "arabe");
	$f3->set("CHIFFRAGE_ROMAIN", "romain");

	$f3->set("THÈME_JOUR", "jour");
	$f3->set("THÈME_NUIT", "nuit");

	$f3->set("RAFRAICHISSEMENT_ASYNCHRONE", "asynchrone");
	$f3->set("RAFRAICHISSEMENT_SYNCHRONE", "synchrone");

	// Et leurs listes complètes
	$f3->set("CALENDRIERS", [
		$f3->get("CALENDRIER_HISTORIQUE"),
		$f3->get("CALENDRIER_RÉVISÉ"),
	]);

	$f3->set("ORTHOGRAPHES", [
		$f3->get("ORTHOGRAPHE_CLASSIQUE"),
		$f3->get("ORTHOGRAPHE_TRADITIONNELLE"),
		$f3->get("ORTHOGRAPHE_MODERNE"),
	]);

	$f3->set("ORDINATIONS", [
		$f3->get("ORDINATION_TRADITIONNELLE"),
		$f3->get("ORDINATION_MODERNE"),
	]);

	$f3->set("CHIFFRAGES", [
		$f3->get("CHIFFRAGE_ARABE"),
		$f3->get("CHIFFRAGE_ROMAIN"),
	]);

	$f3->set("THÈMES", [
		$f3->get("THÈME_JOUR"),
		$f3->get("THÈME_NUIT"),
	]);

	$f3->set("RAFRAICHISSEMENTS", [
		$f3->get("RAFRAICHISSEMENT_ASYNCHRONE"),
		$f3->get("RAFRAICHISSEMENT_SYNCHRONE"),
	]);

	// Puis leurs valeurs par défaut
	$f3->set("CALENDRIER_PAR_DÉFAUT", $f3->get("CALENDRIER_RÉVISÉ"));
	$f3->set("ORTHOGRAPHE_PAR_DÉFAUT", $f3->get("ORTHOGRAPHE_MODERNE"));
	$f3->set("FUSEAU_HORAIRE_PAR_DÉFAUT", "Europe/Paris");
	$f3->set("ORDINATION_PAR_DÉFAUT", $f3->get("ORDINATION_TRADITIONNELLE"));
	$f3->set("CHIFFRAGE_PAR_DÉFAUT", $f3->get("CHIFFRAGE_ARABE"));
	$f3->set("THÈME_PAR_DÉFAUT", $f3->get("THÈME_JOUR"));
	$f3->set("RAFRAICHISSEMENT_PAR_DÉFAUT", $f3->get("RAFRAICHISSEMENT_ASYNCHRONE"));

?>