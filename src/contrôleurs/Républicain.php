<?php

	class Républicain {
		public static function définirEnTêtesSécurité (string $nonce) {
			// En‐têtes de sécurité…
			header("Content-Security-Policy: " .
				"default-src 'none' ; " .
				"style-src 'self' 'unsafe-inline' ; font-src 'self' ; " .
				"script-src 'self' 'unsafe-inline' https: 'nonce-$nonce' 'strict-dynamic' ; " .
				"img-src 'self' ; media-src 'self' ; object-src 'none' ; child-src 'none' ; " .
				"base-uri 'none' ; manifest-src 'self' ; worker-src 'self' ; " .
				"frame-ancestors 'none' ; form-action 'self' ; " . 
				"sandbox allow-same-origin allow-forms allow-scripts allow-popups ; " .
				"prefetch-src 'none' ; connect-src 'self' ; trusted-types 'none' ;"
			);
			header("Cross-Origin-Opener-Policy: same-origin");
			header("Cross-Origin-Resource-Policy: same-origin");
			header("Referrer-Policy: no-referrer");
			header("Strict-Transport-Security: max-age=31536000"); // 365 jours
			header("X-Content-Type-Options: nosniff");
			header("X-DNS-Prefetch-Control: off");
			header("X-Frame-Options: Deny");
			header("X-Permitted-Cross-Domain-Policies: none");
			header("X-Powered-By: ");
		}

		public static function beforeRoute ($f3) {
			// Définit le fuseau horaire
			date_default_timezone_set("Europe/Paris");

			// Définit le nom de la session et ses paramètres
			session_name("IDRepublicain");

			session_set_cookie_params([
				"path" => $f3->get("BASE"),
				"domain" => $f3->get("HOST"),
				"lifetime" => $f3->get("DURÉE_MOUCHARD"),
				"samesite" => "Strict",
				"httponly" => true,
				"secure" => true,
			]);

			// Crée ou récupère une session existante
			session_start();

			// Définit les paramètres de session de l’utilisateur
			$f3->set("PARAMÈTRES", Paramètres::obtenir());

			// Crée ou récupère un jeton CSRF de session
			CSRF::créerJeton($f3->get("DURÉE_LIMITE_JETON"));

			// Définit un nonce pour la page générée
			$f3->set("NONCE", Aléatoire::nonce());

			// Et charge les en‐têtes de sécurité
			Républicain::définirEnTêtesSécurité($f3->get("NONCE"));
		}

		protected static function préparerPageCalendrier (?string $date = null, ?string $heure = null, ?string $calendrier = null, ?string $orthographe = null, ?string $fuseauHoraire = null) : ?array {
			$f3 = \Base::instance();

			// Calcule la date républicaine du jour à partir des données configurées
			return CalendrierRépublicain::obtenirDate($date, $heure, $calendrier ?? $f3->get("PARAMÈTRES")["calendrier"], $orthographe ?? $f3->get("PARAMÈTRES")["orthographe"], $fuseauHoraire ?? $f3->get("PARAMÈTRES")["fuseauHoraire"]);
		}

		public static function accueil ($f3) : void {
			$dateRépublicaine = Républicain::préparerPageCalendrier();

			$f3->set("nomPage", "Date du jour");
			$f3->set("dateRépublicaine", $dateRépublicaine);
			$f3->set("données", $dateRépublicaine["données"]);
			$f3->set("dateCourante", $dateRépublicaine["objetSource"]);
			$f3->set("année", $dateRépublicaine[$f3->get("PARAMÈTRES")["chiffrage"] == $f3->get("CHIFFRAGE_ARABE") ? "annéeChiffresArabes" : "annéeChiffresRomains"]);

			echo Template::instance()->render("accueil.f3v");
		}

		public static function an ($f3) : void {
			// Récupère la date reçue en paramètre
			$date = $_GET["date"] ?? null;

			$dateRépublicaine = Républicain::préparerPageCalendrier($date);

			if ($dateRépublicaine != null) {
				$annéeFormatée = $dateRépublicaine[$f3->get("PARAMÈTRES")["chiffrage"] == $f3->get("CHIFFRAGE_ARABE") ? "annéeChiffresArabes" : "annéeChiffresRomains"];

				$f3->set("nomPage", $date == null ? "Année courante" : ("An " . $annéeFormatée));
				$f3->set("dateRépublicaine", $dateRépublicaine);
				$f3->set("données", $dateRépublicaine["données"]);
				$f3->set("dateCourante", $dateRépublicaine["objetSource"]);
				$f3->set("dateDébutAnnée", $dateRépublicaine["objetDébutAnnée"]);
				$f3->set("dateFinAnnée", $dateRépublicaine["objetFinAnnée"]);
				$f3->set("année", $annéeFormatée);

				echo Template::instance()->render("an.f3v");
			}

			else
				Républicain::erreurDateIncorrecte();
		}

		public static function mois ($f3) : void {
			// Récupère la date reçue en paramètre
			$date = $_GET["date"] ?? null;

			$dateRépublicaine = Républicain::préparerPageCalendrier($date);

			if ($dateRépublicaine != null) {
				if ($dateRépublicaine["mois"] == 13)
					$f3->reroute("@décade?date=$date");

				$annéeFormatée = $dateRépublicaine[$f3->get("PARAMÈTRES")["chiffrage"] == $f3->get("CHIFFRAGE_ARABE") ? "annéeChiffresArabes" : "annéeChiffresRomains"];

				$f3->set("nomPage", $date == null ? "Mois courant" : ($dateRépublicaine["nomMois"] . " " . $annéeFormatée));
				$f3->set("dateRépublicaine", $dateRépublicaine);
				$f3->set("données", $dateRépublicaine["données"]);
				$f3->set("dateCourante", $dateRépublicaine["objetSource"]);
				$f3->set("dateDébutMois", $dateRépublicaine["objetDébutMoisCourant"]);
				$f3->set("dateFinMois", $dateRépublicaine["objetFinMoisCourant"]);
				$f3->set("année", $annéeFormatée);

				echo Template::instance()->render("mois.f3v");
			}

			else
				Républicain::erreurDateIncorrecte();
		}

		public static function décade ($f3) : void {
			// Récupère la date reçue en paramètre
			$date = $_GET["date"] ?? null;

			$dateRépublicaine = Républicain::préparerPageCalendrier($date);

			if ($dateRépublicaine != null) {
				$annéeFormatée = $dateRépublicaine[$f3->get("PARAMÈTRES")["chiffrage"] == $f3->get("CHIFFRAGE_ARABE") ? "annéeChiffresArabes" : "annéeChiffresRomains"];

				$f3->set("nomPage", $date == null ? "Décade courante" : ("Décade de " . $dateRépublicaine["nomMois"] . " " . $annéeFormatée));
				$f3->set("dateRépublicaine", $dateRépublicaine);
				$f3->set("données", $dateRépublicaine["données"]);
				$f3->set("dateCourante", $dateRépublicaine["objetSource"]);
				$f3->set("dateDébutDécade", $dateRépublicaine["objetDébutDécadeCourante"]);
				$f3->set("dateFinDécade", $dateRépublicaine["objetFinDécadeCourante"]);
				$f3->set("année", $annéeFormatée);

				// Dans le cas des sans‐culottides, affiche une vue spéciale
				if ($dateRépublicaine["décadeAnnée"] == 37)
					echo Template::instance()->render("sansCulottides.f3v");

				else
					echo Template::instance()->render("décade.f3v");
			}

			else
				Républicain::erreurDateIncorrecte();
		}

		public static function jour ($f3) : void {
			// Récupère la date reçue en paramètre
			$date = $_GET["date"] ?? null;
			$heure = $_GET["heure"] ?? null;

			$dateRépublicaine = Républicain::préparerPageCalendrier($date, $heure);

			if ($dateRépublicaine != null) {
				$annéeFormatée = $dateRépublicaine[$f3->get("PARAMÈTRES")["chiffrage"] == $f3->get("CHIFFRAGE_ARABE") ? "annéeChiffresArabes" : "annéeChiffresRomains"];

				$f3->set("nomPage", $date == null ? "Jour courant" : ($dateRépublicaine["nomJourDécade"] . " " . $dateRépublicaine["jourMois"] . " " . $dateRépublicaine["nomMois"] . " " . $annéeFormatée));
				$f3->set("dateRépublicaine", $dateRépublicaine);
				$f3->set("dateCourante", $dateRépublicaine["objetSource"]);
				$f3->set("année", $annéeFormatée);

				echo Template::instance()->render("jour.f3v");
			}

			else
				Républicain::erreurDateIncorrecte();
		}

		public static function heure ($f3) : void {
			// Récupère la date reçue en paramètre
			$date = $_GET["date"] ?? null;
			$heure = $_GET["heure"] ?? null;

			$dateRépublicaine = Républicain::préparerPageCalendrier($date, $heure);

			if ($dateRépublicaine != null) {
				$f3->set("nomPage", $heure == null ? "Heure courante" : "Heure exacte");
				$f3->set("dateRépublicaine", $dateRépublicaine);
				$f3->set("dateCourante", $dateRépublicaine["objetSource"]);

				echo Template::instance()->render("heure.f3v");
			}

			else
				Républicain::erreurDateIncorrecte();
		}

		public static function conversion ($f3) : void {
			// Récupère les paramètres reçus pour la date
			$date = $_GET["date"] ?? null;
			$jour = $_GET["jour"] ?? null;
			$mois = $_GET["mois"] ?? null;
			$année = $_GET["an"] ?? null;

			// Récupère les paramètres reçus pour l’heure
			$heure = $_GET["heure"] ?? null;
			$heures = $_GET["heures"] ?? null;
			$minutes = $_GET["minutes"] ?? null;
			$secondes = $_GET["secondes"] ?? null;
			$centisecondes = $_GET["centisecondes"] ?? null;

			// Si les jour, mois et année sont fournis sans date, les conserve tels quels
			if ($jour !== null && $mois !== null && $année !== null && $date === null) {}

			// Si le paramètre de date est fourni sans jour, mois ni année, il est décomposé
			else if ($date !== null && $jour === null && $mois === null && $année === null) {
				// Récupère son format canonique
				$date = OutilsCalendrier::canoniserDate($date);

				// Si la date correspond au format canonique attendu (AAAA-MM-JJ)
				// Récupère les valeurs de la date dans les variables appropriées
				if (preg_match("/^(\d{4})-(\d{2})-(\d{2})$/", $date, $partiesDate, PREG_UNMATCHED_AS_NULL)) {
					$année = $partiesDate[1];
					$mois = $partiesDate[2];
					$jour = $partiesDate[3];
				}

				// Sinon, prépare l’interruption de réponse
				else
					$erreur = true;
			}

			// Sinon, si aucun de ces paramètres n’est fourni, récupère la date du jour
			else if ($jour === null && $mois === null && $année === null && $date === null) {
				$dateRépublicaine = CalendrierRépublicain::obtenirDate(null, null, $f3->get("PARAMÈTRES")["calendrier"], $f3->get("PARAMÈTRES")["orthographe"], $f3->get("PARAMÈTRES")["fuseauHoraire"]);

				$jour = $dateRépublicaine["jourMois"];
				$mois = $dateRépublicaine["mois"];
				$année = $dateRépublicaine["année"];

				// Si en outre les paramètres d’heure ne sont pas fournis, alors récupère l’heure courante
				if ($centisecondes === null && $secondes === null && $minutes === null && $heures === null && $heure === null) {
					$heures = $dateRépublicaine["heures"];
					$minutes = $dateRépublicaine["minutes"];
					$secondes = $dateRépublicaine["secondes"];
					$centisecondes = $dateRépublicaine["centisecondes"];
				}
			}

			// Sinon, la combinaison de paramètres est incorrecte
			else
				$erreur = true;

			// Si les heures et minutes sont fournies sans heure, les conserve telles quelles
			// Et prend des valeurs par défaut pour les secondes et centisecondes si nécessaire
			if ($heures !== null && $minutes !== null && $heure === null) {
				$secondes ??= "00";
				$centisecondes ??= "00";
			}

			// Si le paramètre d’heure est fourni sans heures, minutes, secondes ni centisecondes, il est décomposé
			else if ($heure !== null && $centisecondes === null && $secondes === null && $minutes === null && $heures === null) {
				$heure = OutilsCalendrier::canoniserHeureRépublicaine($heure);

				// Enfin, si l’heure correspond au format canonique attendu (H:MM:SS.mm)
				// Récupère les valeurs de l’heure dans les variables appropriées
				if (preg_match("/^(\d):(\d{2}):(\d{2})\.(\d{2})$/", $heure, $partiesHeure, PREG_UNMATCHED_AS_NULL)) {
					$heures = $partiesHeure[1];
					$minutes = $partiesHeure[2];
					$secondes = $partiesHeure[3];
					$centisecondes = $partiesHeure[4];
				}

				// Sinon, prépare l’interruption de réponse
				else
					$erreur = true;
			}

			// Sinon, si aucun paramètre n’est fourni, définit des valeurs par défaut
			else if ($heure === null && $heures === null && $minutes === null && $secondes === null && $centisecondes === null) {
				$heures = "0";
				$minutes = "00";
				$secondes = "00";
				$centisecondes = "00";
			}

			// Sinon, la combinaison de paramètres est incorrecte
			else
				$erreur = true;

			// Si au moins un paramètre est manquant ou malformé, indique que la date est incorrecte
			if (isset($erreur))
				Républicain::erreurDateIncorrecte();

			// Enfin, construit la date grégorienne à partir des paramètres fournis représentant la date républicaine
			$dateGrégorienne = CalendrierGrégorien::calculerDate($jour, $mois, $année, "$heures:$minutes:$secondes.$centisecondes", $f3->get("PARAMÈTRES")["calendrier"], $f3->get("PARAMÈTRES")["orthographe"], $f3->get("PARAMÈTRES")["fuseauHoraire"]);

			if ($dateGrégorienne === null)
				Républicain::erreurDateIncorrecte();

			else {
				$f3->set("nomPage", "Conversion");
				$f3->set("dateGrégorienne", $dateGrégorienne);
				$f3->set("année", $dateGrégorienne[$f3->get("PARAMÈTRES")["chiffrage"] == $f3->get("CHIFFRAGE_ARABE") ? "annéeChiffresArabes" : "annéeChiffresRomains"]);

				echo Template::instance()->render("conversion.f3v");
			}
		}

		public static function contrôles ($f3) : void {
			$contrôles = SérieContrôles::lancer(__DIR__ . "/contrôles.json", "dateGrégorienne", "dateRépublicaine", "formatRéponseRépublicain", function (array $dateGrégorienne, array $contrôle, array $listeContrôle) use ($f3) {
				return CalendrierRépublicain::obtenirDate(
					"{$dateGrégorienne["année"]}-{$dateGrégorienne["mois"]}-{$dateGrégorienne["jourMois"]}",
					"{$dateGrégorienne["heures"]}:{$dateGrégorienne["minutes"]}:{$dateGrégorienne["secondes"]}.{$dateGrégorienne["millisecondes"]}",
					$f3->get($contrôle["calendrier"] ?? $listeContrôle["calendrier"] ?? "CALENDRIER_PAR_DÉFAUT"),
					$f3->get($contrôle["orthographe"] ?? $listeContrôle["orthographe"] ?? "ORTHOGRAPHE_PAR_DÉFAUT"),
					$contrôle["fuseauHoraire"] ?? $listeContrôle["fuseauHoraire"] ?? $f3->get("FUSEAU_HORAIRE_PAR_DÉFAUT")
				);
			});

			$rétrocontrôles = SérieContrôles::lancer(__DIR__ . "/contrôles.json", "dateRépublicaine", "dateGrégorienne", "formatRéponseGrégorien", function (array $dateRépublicaine, array $contrôle, array $listeContrôle) use ($f3) {
				return CalendrierGrégorien::calculerDate(
					$dateRépublicaine["jourMois"],
					$dateRépublicaine["mois"],
					$dateRépublicaine["année"],
					"{$dateRépublicaine["heures"]}:{$dateRépublicaine["minutes"]}:{$dateRépublicaine["secondes"]}.{$dateRépublicaine["centisecondes"]}",
					$f3->get($contrôle["calendrier"] ?? $listeContrôle["calendrier"] ?? "CALENDRIER_PAR_DÉFAUT"),
					$f3->get($contrôle["orthographe"] ?? $listeContrôle["orthographe"] ?? "ORTHOGRAPHE_PAR_DÉFAUT"),
					$contrôle["fuseauHoraire"] ?? $listeContrôle["fuseauHoraire"] ?? $f3->get("FUSEAU_HORAIRE_PAR_DÉFAUT")
				);
			});

			$f3->set("nomPage", "Contrôles unitaires");
			$f3->set("contrôles", $contrôles);
			$f3->set("rétrocontrôles", $rétrocontrôles);

			echo Template::instance()->render("contrôles.f3v");
		}

		public static function tableauAnnées ($f3) : void {
			$années = [];

			$dateCourante = "1692-09-22";

			for ($i = 0; $i < 500; $i++) {
				$dateRépublicaine = CalendrierRépublicain::obtenirDate($dateCourante, null, $f3->get("PARAMÈTRES")["calendrier"], $f3->get("PARAMÈTRES")["orthographe"], $f3->get("PARAMÈTRES")["fuseauHoraire"]);

				$années[$i] = [
					"année" => $dateRépublicaine["annéeChiffresArabes"],
					"jourDébut" => $dateRépublicaine["débutAnnée"],
					"jourFin" => $dateRépublicaine["finAnnée"],
					"sextile" => $dateRépublicaine["annéeSextile"],
				];

				$dateCourante = $dateRépublicaine["annéeSuivante"];
			}

			$f3->set("nomPage", "Tableau des années");
			$f3->set("années", $années);

			echo Template::instance()->render("tableauAnnées.f3v");
		}

		public static function impression ($f3) {
			// Récupère la date reçue en paramètre
			$date = $_GET["date"] ?? null;

			$dateRépublicaine = Républicain::préparerPageCalendrier($date);

			if ($dateRépublicaine != null) {
				$annéeFormatée = $dateRépublicaine[$f3->get("PARAMÈTRES")["chiffrage"] == $f3->get("CHIFFRAGE_ARABE") ? "annéeChiffresArabes" : "annéeChiffresRomains"];

				$f3->set("nomPage", $date == null ? "Impression de l’année courante" : ("Impression de l’an " . $annéeFormatée));
				$f3->set("dateRépublicaine", $dateRépublicaine);
				$f3->set("données", $dateRépublicaine["données"]);
				$f3->set("dateCourante", $dateRépublicaine["objetSource"]);
				$f3->set("dateDébutAnnée", $dateRépublicaine["objetDébutAnnée"]);
				$f3->set("dateFinAnnée", $dateRépublicaine["objetFinAnnée"]);
				$f3->set("année", $annéeFormatée);

				echo Template::instance()->render("impression.f3v");
			}

			else
				Républicain::erreurDateIncorrecte();
		}
		public static function histoire ($f3) : void {
			echo Template::instance()->render("histoire.f3v");
		}

		public static function commentaire ($f3) : void {
			echo Template::instance()->render("commentaire.f3v");
		}

		public static function fichiersInterface ($f3) : void {
			echo Template::instance()->render("fichiersInterface.f3v");
		}

		public static function contact ($f3) : void {
			echo Template::instance()->render("contact.f3v");
		}

		public static function options ($f3) : void {
			echo Template::instance()->render("options.f3v");
		}

		public static function configurerOptions ($f3) : void {
			// Si le jeton CSRF reçu est valide
			if (CSRF::validerJeton($_POST["jetonCSRF"]))
				Paramètres::configurer();

			// Renvoie sur la page d’origine pour empêcher le renvoi de formulaire
			http_response_code(200);
			$f3->reroute("@options");
		}

		public static function restaurerOptions ($f3) : void {
			// Si le jeton CSRF reçu est valide
			if (CSRF::validerJeton($_POST["jetonCSRF"]))
				Paramètres::restaurer();

			// Renvoie sur la page d’origine pour empêcher le renvoi de formulaire
			http_response_code(200);
			$f3->reroute("@options");
		}

		public static function interfaceApplicative ($f3) : void {
			// Récupère la date et l’heure reçues en paramètre
			$date = $_GET["date"] ?? null;
			$heure = $_GET["heure"] ?? null;
			$calendrier = $_GET["calendrier"] ?? null;
			$orthographe = $_GET["orthographe"] ?? null;
			$fuseauHoraire = $_GET["fuseauHoraire"] ?? null;

			$dateRépublicaine = Républicain::préparerPageCalendrier($date, $heure, $calendrier, $orthographe, $fuseauHoraire);

			header("Content-type: application/json");
			header("Access-Control-Allow-Origin: *");
			echo json_encode($dateRépublicaine);
		}

		public static function redirigerInterfaceApplicative ($f3) : void {
			$f3->reroute("@interfaceApplicative");
		}

		public static function manifeste ($f3) : void {
			header("Content-type: application/json");
			echo file_get_contents("ressources/application/manifeste.json");
		}

		public static function serviceWorker ($f3) : void {
			header("Content-type: application/javascript");
			echo file_get_contents("ressources/application/serviceWorker.js");
		}

		public static function erreurDateIncorrecte () : void {
			http_response_code(400);
			echo Template::instance()->render("dateIncorrecte.f3v");
			die();
		}

		public static function erreur (int $statutHTTP = 404) : void {
			http_response_code($statutHTTP);
			echo Template::instance()->render("erreur{$statutHTTP}.f3v");
			die();
		}

		public static function pageInexistante ($f3) : void {
			Républicain::erreur(404);
		}
	}

?>