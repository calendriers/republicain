<?php

	class Paramètres {
		public static function filtrerValeur (array $valeursDemandées, array $valeursPermises, $valeurParDéfaut = null) {
			foreach ($valeursDemandées as $valeurDemandée)
				if (in_array($valeurDemandée, $valeursPermises, true))
					return $valeurDemandée;

			return $valeurParDéfaut;
		}

		public static function filtrerValeurTémoinObtention (string $clé, string $cléParDéfaut, string $cléListeValeursPermises) {
			$f3 = \Base::instance();

			// Récupère la première valeur permise parmi le champ du formulaire, le témoin enregistré ou la valeur par défaut
			return Paramètres::filtrerValeur([ $_COOKIE[$clé] ?? null, $f3->get($cléParDéfaut), ], $f3->get($cléListeValeursPermises));
		}

		public static function filtrerValeurTémoinConfiguration (string $clé, string $cléParDéfaut, string $cléListeValeursPermises) {
			$f3 = \Base::instance();

			// Récupère la première valeur permise parmi le champ du formulaire, le témoin enregistré ou la valeur par défaut
			return Paramètres::filtrerValeur([ $_POST[$clé] ?? null, $_COOKIE[$clé] ?? null, $f3->get($cléParDéfaut), ], $f3->get($cléListeValeursPermises));
		}

		public static function générerTémoin (string $nomTémoin, $valeur, int $durée) : void {
			$f3 = \Base::instance();

			// Si le témoin n’existe pas ou que la nouvelle valeur est différente, initialise le témoin pour une durée donnée
			if (!isset($_COOKIE[$nomTémoin]) || $_COOKIE[$nomTémoin] !== $valeur) {
				setcookie($nomTémoin, $valeur, [
					"expires" => time() + $durée,
					"path" => $f3->get("BASE"),
					"domain" => $f3->get("HOST"),
					"samesite" => "Strict",
					"httponly" => true,
					"secure" => true,
				]);
			}
		}

		public static function obtenir () : array {
			$f3 = \Base::instance();

			// Récupère les valeurs des témoins ou par défaut
			$calendrier = Paramètres::filtrerValeurTémoinObtention("calendrier", "CALENDRIER_PAR_DÉFAUT", "CALENDRIERS");
			$orthographe = Paramètres::filtrerValeurTémoinObtention("orthographe", "ORTHOGRAPHE_PAR_DÉFAUT", "ORTHOGRAPHES");
			$fuseauHoraire = Paramètres::filtrerValeurTémoinObtention("fuseauHoraire", "FUSEAU_HORAIRE_PAR_DÉFAUT", "FUSEAUX_HORAIRES");
			$ordination = Paramètres::filtrerValeurTémoinObtention("ordination", "ORDINATION_PAR_DÉFAUT", "ORDINATIONS");
			$chiffrage = Paramètres::filtrerValeurTémoinObtention("chiffrage", "CHIFFRAGE_PAR_DÉFAUT", "CHIFFRAGES");
			$thème = Paramètres::filtrerValeurTémoinObtention("thème", "THÈME_PAR_DÉFAUT", "THÈMES");
			$rafraichissement = Paramètres::filtrerValeurTémoinObtention("rafraichissement", "RAFRAICHISSEMENT_PAR_DÉFAUT", "RAFRAICHISSEMENTS");

			// Régénère les témoins
			Paramètres::générerTémoin("calendrier", $calendrier, $f3->get("DURÉE_TÉMOIN"));
			Paramètres::générerTémoin("orthographe", $orthographe, $f3->get("DURÉE_TÉMOIN"));
			Paramètres::générerTémoin("fuseauHoraire", $fuseauHoraire, $f3->get("DURÉE_TÉMOIN"));
			Paramètres::générerTémoin("ordination", $ordination, $f3->get("DURÉE_TÉMOIN"));
			Paramètres::générerTémoin("chiffrage", $chiffrage, $f3->get("DURÉE_TÉMOIN"));
			Paramètres::générerTémoin("thème", $thème, $f3->get("DURÉE_TÉMOIN"));
			Paramètres::générerTémoin("rafraichissement", $rafraichissement, $f3->get("DURÉE_TÉMOIN"));

			return [
				"calendrier" => $calendrier,
				"orthographe" => $orthographe,
				"fuseauHoraire" => $fuseauHoraire,
				"ordination" => $ordination,
				"chiffrage" => $chiffrage,
				"thème" => $thème,
				"rafraichissement" => $rafraichissement,
			];
		}

		public static function configurer () : void {
			$f3 = \Base::instance();

			// Récupère la première valeur permise parmi le champ du formulaire, le témoin ou la valeur par défaut
			$calendrier = Paramètres::filtrerValeurTémoinConfiguration("calendrier", "CALENDRIER_PAR_DÉFAUT", "CALENDRIERS");
			$orthographe = Paramètres::filtrerValeurTémoinConfiguration("orthographe", "ORTHOGRAPHE_PAR_DÉFAUT", "ORTHOGRAPHES");
			$fuseauHoraire = Paramètres::filtrerValeurTémoinConfiguration("fuseauHoraire", "FUSEAU_HORAIRE_PAR_DÉFAUT", "FUSEAUX_HORAIRES");
			$ordination = Paramètres::filtrerValeurTémoinConfiguration("ordination", "ORDINATION_PAR_DÉFAUT", "ORDINATIONS");
			$chiffrage = Paramètres::filtrerValeurTémoinConfiguration("chiffrage", "CHIFFRAGE_PAR_DÉFAUT", "CHIFFRAGES");
			$thème = Paramètres::filtrerValeurTémoinConfiguration("thème", "THÈME_PAR_DEFAUT", "THÈMES");
			$rafraichissement = Paramètres::filtrerValeurTémoinConfiguration("rafraichissement", "RAFRAICHISSEMENT_PAR_DÉFAUT", "RAFRAICHISSEMENTS");

			// Puis régénère les témoins
			Paramètres::générerTémoin("calendrier", $calendrier, $f3->get("DURÉE_TÉMOIN"));
			Paramètres::générerTémoin("orthographe", $orthographe, $f3->get("DURÉE_TÉMOIN"));
			Paramètres::générerTémoin("fuseauHoraire", $fuseauHoraire, $f3->get("DURÉE_TÉMOIN"));
			Paramètres::générerTémoin("ordination", $ordination, $f3->get("DURÉE_TÉMOIN"));
			Paramètres::générerTémoin("chiffrage", $chiffrage, $f3->get("DURÉE_TÉMOIN"));
			Paramètres::générerTémoin("thème", $thème, $f3->get("DURÉE_TÉMOIN"));
			Paramètres::générerTémoin("rafraichissement", $rafraichissement, $f3->get("DURÉE_TÉMOIN"));
		}

		public static function restaurer () : void {
			$f3 = \Base::instance();

			// Puis régénère les témoins sur les valeurs par défaut
			Paramètres::générerTémoin("calendrier", $f3->get("CALENDRIER_PAR_DÉFAUT"), $f3->get("DURÉE_TÉMOIN"));
			Paramètres::générerTémoin("orthographe", $f3->get("ORTHOGRAPHE_PAR_DÉFAUT"), $f3->get("DURÉE_TÉMOIN"));
			Paramètres::générerTémoin("fuseauHoraire", $f3->get("FUSEAU_HORAIRE_PAR_DÉFAUT"), $f3->get("DURÉE_TÉMOIN"));
			Paramètres::générerTémoin("ordination", $f3->get("ORDINATION_PAR_DÉFAUT"), $f3->get("DURÉE_TÉMOIN"));
			Paramètres::générerTémoin("chiffrage", $f3->get("CHIFFRAGE_PAR_DÉFAUT"), $f3->get("DURÉE_TÉMOIN"));
			Paramètres::générerTémoin("thème", $f3->get("THÈME_PAR_DÉFAUT"), $f3->get("DURÉE_TÉMOIN"));
			Paramètres::générerTémoin("rafraichissement", $f3->get("RAFRAICHISSEMENT_PAR_DÉFAUT"), $f3->get("DURÉE_TÉMOIN"));
		}
	}

?>