<?php

	class CalendrierRépublicain {
		// Indique si une année républicaine est sextile selon sa variante
		public static function estSextile (int $année, bool $modeRévisé) : bool {
			// Prendre en compte le cas des années négatives
			if ($modeRévisé)
				return CalendrierGrégorien::estBissextile($année);

			else {
				// On peut observer sur les tables de correspondances un cycle qu’on peut faire démarrer à partir de l’an 20 et qui est le suivant : une année sextile produit une franciade quinquenale aux bout de huit franciades, puis sept, huit, sept et huit et on recommence…
				// Pour les années antérieures à l’an 20 et l’espace proleptique, on applique le même cycle en décrémentant le compteur des années ; le cycle étant symétrique, on peut le parcourir dans le même sens et donc simplifier le code
				$cycle = [ 8, 7, 8, 7, 8, ];
				$positionCycle = 0;
				$i = 0;

				if ($année >= 20) {
					$annéeCalcul = 20;
					$sensProgression = 1;
				}

				else {
					$annéeCalcul = 15;
					$sensProgression = -1;
				}

				while (true) {
					if ($i == $cycle[$positionCycle]) {
						$annéeCalcul += $sensProgression;
						$i = 0;
						$positionCycle++;

						if ($positionCycle == 5)
							$positionCycle = 0;
					}

					else {
						if (
							($sensProgression ==  1 && $annéeCalcul >= $année) ||
							($sensProgression == -1 && $annéeCalcul <= $année)
						) {
							break;
						}

						$annéeCalcul += 4 * $sensProgression;
						$i++;
					}
				}

				return $annéeCalcul == $année;
			}
		}

		public static function estimerJourCommencementAnnée (int $annéeGrégorienne, bool $modeRévisé) : int {
			$f3 = \Base::instance();

			// Calcule l’écart sur lequel mesurer le décalage
			$écartDurée = $annéeGrégorienne - $f3->get("ANNÉE_COMMENCEMENT");

			$annéeRépublicaineCourante = 1;
			$annéeGrégorienneCourante = $f3->get("ANNÉE_COMMENCEMENT");

			// Par défaut, l’année républicaine commence un 22 septembre d’une année non bissextile
			$jourCommencement = $f3->get("JOUR_COMMENCEMENT");

			// Si la date républicaine est positive
			if ($écartDurée >= 0) {
				// Or, l’an 1792, où commence le calendrier, est bissextil
				$jourCommencement++;

				do {
					// Si l’année grégorienne est bissextile et l’année républicaine précédente sextile, le jour de commencement avance
					if (!CalendrierGrégorien::estBissextile($annéeGrégorienneCourante) && CalendrierRépublicain::estSextile($annéeRépublicaineCourante - 1, $modeRévisé))
						$jourCommencement++;

					// Si l’inverse se produit, le jour de commencement recule
					else if (CalendrierGrégorien::estBissextile($annéeGrégorienneCourante) && !CalendrierRépublicain::estSextile($annéeRépublicaineCourante - 1, $modeRévisé))
						$jourCommencement--;

					// Passe à l’année suivante et diminue l’écart
					$annéeRépublicaineCourante++;
					$annéeGrégorienneCourante++;
					$écartDurée--;
				}

				// Tant qu’on n’est pas arrivé à la date courante…
				while ($écartDurée >= 0);

				// Enfin, retourne le jour de commencement calculé, augmenté d’un si l’année demandée est bissextile
				return $jourCommencement + CalendrierGrégorien::estBissextile($annéeGrégorienne);
			}

			// Si la date républicaine est négative (proleptique)
			else {
				// Il n’y a pas d’an 0 donc l’année saute à −1
				$annéeRépublicaineCourante = -1;

				do {
					// Si l’année grégorienne est bissextile et l’année républicaine sextile, le jour de commencement recule
					if (!CalendrierGrégorien::estBissextile($annéeGrégorienneCourante) && CalendrierRépublicain::estSextile($annéeRépublicaineCourante, $modeRévisé))
						$jourCommencement--;

					// Si l’inverse se produit, le jour de commencement avance
					else if (CalendrierGrégorien::estBissextile($annéeGrégorienneCourante) && !CalendrierRépublicain::estSextile($annéeRépublicaineCourante, $modeRévisé))
						$jourCommencement++;

					// Passe à l’année précédente
					$annéeRépublicaineCourante--;
					$annéeGrégorienneCourante--;

					// Si l’année grégorienne devient nulle, saute à −1
					if ($annéeGrégorienneCourante == 0)
						$annéeGrégorienneCourante = -1;

					// Et accroit l’écart, négatif
					$écartDurée++;
				}

				// Tant qu’on n’est pas arrivé à la date courante…
				while ($écartDurée < 0);

				// Enfin, retourne le jour de commencement calculé
				return $jourCommencement + CalendrierGrégorien::estBissextile($annéeGrégorienneCourante);
			}
		}

		public static function calculerDate (array $dateGrégorienne, bool $modeRévisé) : array {
			$f3 = \Base::instance();

			$annéeGrégorienne = $dateGrégorienne["année"];
			$jourGrégorien = $dateGrégorienne["jourAnnée"];

			// Estime le jour de commencement de l’année républicaine correspondante
			$jourCommencement = CalendrierRépublicain::estimerJourCommencementAnnée($annéeGrégorienne, $modeRévisé);

			// Initialise les années grégorienne et républicaine s’équivalant
			$annéeGrégorienneCourante = $annéeGrégorienne;
			$annéeRépublicaineCourante = $annéeGrégorienneCourante - $f3->get("ANNÉE_COMMENCEMENT") + 1;

			// Si ce jour grégorien précède celui de début de l’année républicaine associée à cette année
			if ($jourGrégorien < $jourCommencement) {
				// Diminue les années républicaine et grégorienne
				$annéeGrégorienneCourante--;
				$annéeRépublicaineCourante--;

				// Et recalcule le jour de départ de l’année républicaine réelle
				$jourCommencement = CalendrierRépublicain::estimerJourCommencementAnnée($annéeGrégorienneCourante, $modeRévisé);
			}

			$annéeCommencement = $annéeGrégorienneCourante;

			// Initialise le jour républicain de l’an et le jour grégorien
			$jourRépublicainCourant = 1;
			$jourGrégorienCourant = $jourCommencement;

			// Tant que le jour et l’année ne concordent pas, passe une journée
			while ($jourGrégorienCourant < $jourGrégorien || $annéeGrégorienneCourante < $annéeGrégorienne) {
				$jourRépublicainCourant++;
				$jourGrégorienCourant++;
	 
				// Si le jour de l’année déborde selon que l’année est bissextile ou non, incrémente l’année et remet le jour à 1
				if ((CalendrierGrégorien::estBissextile($annéeGrégorienneCourante) && $jourGrégorienCourant > $f3->get("JOURS_PAR_AN_BISSEXTIL")) || (!CalendrierGrégorien::estBissextile($annéeGrégorienneCourante) && $jourGrégorienCourant > $f3->get("JOURS_PAR_AN")))  {
					$annéeGrégorienneCourante += 1;
					$jourGrégorienCourant = 1;
				}
			}

			// Si l’année est proleptique, elle est diminuée d’un pour sauter l’an zéro, absent des calendriers civils
			$annéeRépublicaineCourante = $annéeRépublicaineCourante > 0 ? $annéeRépublicaineCourante : $annéeRépublicaineCourante - 1;

			// Objet décrivant les caractéristiques de la date républicaine
			return [
				"année" => $annéeRépublicaineCourante,
				"annéeSextile" => CalendrierRépublicain::estSextile($annéeRépublicaineCourante, $modeRévisé),
				"jourAnnée" => $jourRépublicainCourant,

				// Équivalents grégoriens
				"jourCommencement" => $jourCommencement,
				"annéeCommencement" => $annéeCommencement,
			];
		}

		public static function obtenirDate (?string $date, ?string $heure, string $calendrier, string $orthographe, string $fuseauHoraire) : ?array {
			$f3 = \Base::instance();

			// Charge les données
			$données = JSON::chargerDonnées($orthographe);

			// Récupère la date et l’heure du jour
			$dateCourante = CalendrierGrégorien::obtenirDate(CalendrierGrégorien::obtenirTemps($date, $heure, $fuseauHoraire), $données);

			// Si la date récupérée est invalide, ou qu’un des paramètres reçus est inconnu, interrompt la demande
			if (
				$dateCourante == null ||
				!in_array($calendrier, $f3->get("CALENDRIERS")) ||
				!in_array($orthographe, $f3->get("ORTHOGRAPHES")) ||
				!in_array($fuseauHoraire, $f3->get("FUSEAUX_HORAIRES"))
			) {
				return null;
			}

			else {
				$dateRépublicaine = [];

				/***************************************************************/
				/************ Calcul des informations fondamentales ************/
				/***************************************************************/

				// Objet décrivant une date dans le calendrier républicain (année, jour et sextilité)
				$informationsDateRépublicaine = CalendrierRépublicain::calculerDate($dateCourante, ($calendrier == $f3->get("CALENDRIER_RÉVISÉ")));

				$dateRépublicaine["objetSource"] = $dateCourante;

				$jourCommencement = $informationsDateRépublicaine["jourCommencement"];
				$annéeCommencement = $informationsDateRépublicaine["annéeCommencement"];

				// Prend le jour courant comme date de référence
				$référenceAnnée = $annéeCommencement . "-01-01";
				// La date de référence est en décalage d’autant de jours que la position de la date de début attendue dans le calendrier grégorien, moins 1 car le calendrier commence avec 1
				$nombreJoursDécalage = $jourCommencement - 1;

				/***************************************************************/
				/************* Calcul des informations sur l’année *************/
				/***************************************************************/

				$dateRépublicaine["année"] = $informationsDateRépublicaine["année"];
				$dateRépublicaine["annéeChiffresArabes"] = Format::année($informationsDateRépublicaine["année"], $f3->get("CHIFFRAGE_ARABE"));
				$dateRépublicaine["annéeChiffresRomains"] = Format::année($informationsDateRépublicaine["année"], $f3->get("CHIFFRAGE_ROMAIN"));
				$dateRépublicaine["annéeSextile"] = $informationsDateRépublicaine["annéeSextile"];

				/***************************************************************/
				/********************** Calcul de l’heure **********************/
				/***************************************************************/

				// Produit en croix pour obtenir le nombre de centisecondes républicaines écoulées
				// Note : on ne garde une précision qu’à la centiseconde car la milliseconde républicaine à une précision inférieure à la milliseconde grégorienne, sur laquelle on souhaite s’étalonner (notamment du fait de la restriction de précision des navigateurs)
				// On omet le facteur 1 000 des millisecondes puisqu’il est commun au numérateur et dénominateur
				// Mais on veut en plus ramener ce nombre à des centisecondes donc on multiplie le dénominateur par 10
				$centisecondesJournée = strval(sprintf("%07d", round(($dateCourante["millisecondesJournée"] * $f3->get("SECONDES_PAR_JOUR_RÉPUBLICAIN")) / ($f3->get("SECONDES_PAR_JOUR") * 10))));

				// Découpe la chaine pour en récupérer les valeurs par champs
				$dateRépublicaine["heures"] = intval($centisecondesJournée[0]);
				$dateRépublicaine["minutes"] = intval(substr($centisecondesJournée, 1, 2));
				$dateRépublicaine["secondes"] = intval(substr($centisecondesJournée, 3, 2));
				$dateRépublicaine["centisecondes"] = intval(substr($centisecondesJournée, 5, 2));

				$dateRépublicaine["secondesJournée"] = intdiv($centisecondesJournée, $f3->get("CENTISECONDES_PAR_SECONDE_RÉPUBLICAINE"));
				$dateRépublicaine["centisecondesJournée"] = intval($centisecondesJournée);

				/***************************************************************/
				/************* Calcul des informations sur le jour *************/
				/***************************************************************/

				$dateRépublicaine["jourAnnée"] = $informationsDateRépublicaine["jourAnnée"];

				// Puis on détermine le nom du jour de l’année
				$dateRépublicaine["informationsNomJourAnnée"] = $données["joursAnnée"][$dateRépublicaine["jourAnnée"] - 1];
				$dateRépublicaine["nomJourAnnée"] = Format::nomJour($dateRépublicaine["informationsNomJourAnnée"], $données["préfixes"]);

				// Le numéro du jour du mois
				$dateRépublicaine["jourMois"] = ($dateRépublicaine["jourAnnée"] - 1) % $f3->get("JOURS_PAR_MOIS_RÉPUBLICAIN") + 1;

				// Le numéro du jour de la décade (transforme en chaine retournée pour n’en garde que le premier chiffre…)
				$dateRépublicaine["jourDécade"] = $dateRépublicaine["jourMois"] % 10;

				// Le nom du jour de la décade
				$dateRépublicaine["nomJourDécade"] = $données["nomsJoursDécade"][($dateRépublicaine["jourMois"] - 1) % count($données["nomsJoursDécade"])];

				/***************************************************************/
				/************* Calcul des informations sur le mois *************/
				/***************************************************************/

				// Le numéro et le nom du mois
				$dateRépublicaine["mois"] = intdiv($informationsDateRépublicaine["jourAnnée"] - 1, $f3->get("JOURS_PAR_MOIS_RÉPUBLICAIN")) + 1;
				$dateRépublicaine["nomMois"] = $données["nomsMois"][$dateRépublicaine["mois"] - 1];

				// La description du mois de l’année
				$dateRépublicaine["descriptionMois"] = $données["descriptionsMois"][$dateRépublicaine["mois"] - 1];
				$dateRépublicaine["descriptionMoisHTML"] = str_replace("\n", "<br />", $dateRépublicaine["descriptionMois"]);

				// Le nom du fichier du mois
				$fichierMois = $données["nomsImagesMois"][$dateRépublicaine["mois"] - 1];

				$dateRépublicaine["intituléImageMois"] = $données["préfixes"]["intituléImageMois"] . $dateRépublicaine["nomMois"];

				$dateRépublicaine["adresseRelativeImageMois"] = Format::adresseRelativeImage($données, $fichierMois);
				$dateRépublicaine["adresseRelativeImageCompresséeMois"] = Format::adresseRelativeImage($données, $fichierMois . "-min");
				$dateRépublicaine["adresseComplèteImageMois"] = Format::adresseComplèteImage($données, $fichierMois);
				$dateRépublicaine["adresseComplèteImageCompresséeMois"] = Format::adresseComplèteImage($données, $fichierMois . "-min");

				// Le numéro de la décade du mois
				$dateRépublicaine["décadeMois"] = intdiv($dateRépublicaine["jourMois"] - 1, 10) + 1;

				// Le numéro de la décade de l’année
				$dateRépublicaine["décadeAnnée"] = intdiv($dateRépublicaine["jourAnnée"] - 1, 10) + 1;

				if ($dateRépublicaine["jourDécade"] == 0)
					$dateRépublicaine["jourDécade"] = 10;

				/***************************************************************/
				/************* Calcul des débuts et fin de l’année *************/
				/***************************************************************/

				// Détermine la date grégorienne correspondant au début de cette année républicaine
				$dateGrégorienneDébut = (new DateTimeImmutable($référenceAnnée))->modify("+" . $nombreJoursDécalage . " days");

				$dateRépublicaine["débutAnnée"] = $dateGrégorienneDébut->format("Y-m-d");

				// Détermine la date grégorienne correspondant au début de cette année républicaine
				$dateRépublicaine["finAnnée"] = $dateGrégorienneDébut
					// Ajoute autant de jours que cette année républicaine en a, moins 1 pour ne boucler pas
					->modify("+" . (($informationsDateRépublicaine["annéeSextile"] ? $f3->get("JOURS_PAR_AN_BISSEXTIL") : $f3->get("JOURS_PAR_AN")) - 1) . " days")
					->format("Y-m-d");

				// Récupère des objets décrivant ces deux dates
				$dateRépublicaine["objetDébutAnnée"] = CalendrierGrégorien::obtenirDate(OutilsCalendrier::tempsUNIX($dateRépublicaine["débutAnnée"]), $données, false);
				$dateRépublicaine["objetFinAnnée"] = CalendrierGrégorien::obtenirDate(OutilsCalendrier::tempsUNIX($dateRépublicaine["finAnnée"]), $données, false);

				/***************************************************************/
				/******** Calcul des débuts et fins des mois et décades ********/
				/***************************************************************/

				$dateRépublicaine["débutsMois"] = [];
				$dateRépublicaine["finsMois"] = [];
				$dateRépublicaine["débutsDécades"] = [];
				$dateRépublicaine["finsDécades"] = [];

				// Initialise le compteur de jours
				$dateGrégorienneDébutDécades = (new DateTime($référenceAnnée))->modify("+" . $nombreJoursDécalage . " days");

				// Définit les premières valeurs
				$dateRépublicaine["débutsMois"][0] = $dateGrégorienneDébutDécades->format("Y-m-d");
				$dateRépublicaine["débutsDécades"][0] = $dateRépublicaine["débutsMois"][0];

				// Pour chaque décade de l’année
				for ($numéroDécade = 1; $numéroDécade < $f3->get("DÉCADES_PAR_AN"); $numéroDécade++) {
					// Ajoute au compteur de jours autant de jours que nécessaire
					// Puis formate la date pour définir la fin de la décade
					$dateGrégorienneDébutDécades->modify("+" . ($f3->get("JOURS_PAR_DÉCADE") - 1) . " days");
					$dateRépublicaine["finsDécades"][$numéroDécade - 1] = $dateGrégorienneDébutDécades->format("Y-m-d");

					// Et de même avec le début de la décade suivante
					$dateGrégorienneDébutDécades->modify("+1 day");
					$dateRépublicaine["débutsDécades"][$numéroDécade] = $dateGrégorienneDébutDécades->format("Y-m-d");

					$numéroMois = $numéroDécade / 3;

					// Et si le nombre est multiple entier de 3
					if (is_int($numéroMois)) {
						// Définit la fin du mois précédent et le début du mois courant
						$dateRépublicaine["finsMois"][$numéroMois - 1] = $dateRépublicaine["finsDécades"][$numéroDécade - 1];
						$dateRépublicaine["débutsMois"][$numéroMois] = $dateRépublicaine["débutsDécades"][$numéroDécade];
					}
				}

				$dateRépublicaine["finsDécades"][] = $dateRépublicaine["finAnnée"];
				$dateRépublicaine["finsMois"][] = $dateRépublicaine["finAnnée"];

				$dateRépublicaine["débutMoisCourant"] = $dateRépublicaine["débutsMois"][$dateRépublicaine["mois"] - 1];
				$dateRépublicaine["finMoisCourant"] = $dateRépublicaine["finsMois"][$dateRépublicaine["mois"] - 1];

				// Récupère des objets décrivant ces deux dates
				$dateRépublicaine["objetDébutMoisCourant"] = CalendrierGrégorien::obtenirDate(OutilsCalendrier::tempsUNIX($dateRépublicaine["débutMoisCourant"]), $données, false);
				$dateRépublicaine["objetFinMoisCourant"] = CalendrierGrégorien::obtenirDate(OutilsCalendrier::tempsUNIX($dateRépublicaine["finMoisCourant"]), $données, false);

				$dateRépublicaine["débutDécadeCourante"] = $dateRépublicaine["débutsDécades"][$dateRépublicaine["décadeAnnée"] - 1];
				$dateRépublicaine["finDécadeCourante"] = $dateRépublicaine["finsDécades"][$dateRépublicaine["décadeAnnée"] - 1];

				// Récupère des objets décrivant ces deux dates
				$dateRépublicaine["objetDébutDécadeCourante"] = CalendrierGrégorien::obtenirDate(OutilsCalendrier::tempsUNIX($dateRépublicaine["débutDécadeCourante"]), $données, false);
				$dateRépublicaine["objetFinDécadeCourante"] = CalendrierGrégorien::obtenirDate(OutilsCalendrier::tempsUNIX($dateRépublicaine["finDécadeCourante"]), $données, false);

				/****************************************************************/
				/**************** Rappel des paramètres d’entrée ****************/
				/****************************************************************/

				$annéePrécédenteSextile = CalendrierRépublicain::estSextile($dateRépublicaine["année"] - 1, $calendrier == $f3->get("CALENDRIER_RÉVISÉ"));

				$dateRépublicaine["jourPrécédent"] = (new DateTime($dateCourante["dateChaineTiretsInversée"]))->modify("-1 day")->format("Y-m-d");
				$dateRépublicaine["jourSuivant"] = (new DateTime($dateCourante["dateChaineTiretsInversée"]))->modify("+1 day")->format("Y-m-d");

				$dateRépublicaine["décadePrécédente"] = (new DateTime($dateRépublicaine["débutDécadeCourante"]))
					->modify(
						$dateRépublicaine["décadeAnnée"] == 1
						? (
							$annéePrécédenteSextile
							? "-6 days"
							: "-5 days"
						)
						: "-" . $f3->get("JOURS_PAR_DÉCADE") . " days"
					)
					->format("Y-m-d");

				// Quoi qu’il arrive, la décade suivante commence un jour après la date de fin de la décade courante
				$dateRépublicaine["décadeSuivante"] = (new DateTime($dateRépublicaine["finDécadeCourante"]))
					->modify("+1 day")
					->format("Y-m-d");

				$dateRépublicaine["moisPrécédent"] = (new DateTime($dateRépublicaine["débutMoisCourant"]))
					// Recule de trente jours
					->modify("-" . $f3->get("JOURS_PAR_MOIS_RÉPUBLICAIN") . " days")
					->modify(
						// Si le mois courant est le premier
						$dateRépublicaine["mois"] == 1
						// Saute les Sans‐culottides
						? (
							$annéePrécédenteSextile
							? "-6 days"
							: "-5 days"
						)
						// Sinon, ne fait rien de plus
						: "-0 day"
					)
					->format("Y-m-d");

				$dateRépublicaine["moisSuivant"] = (new DateTime($dateRépublicaine["finMoisCourant"]))
					// Avance d’un jour
					->modify("+1 day")
					->modify(
						// Si le mois courant est le dernier
						$dateRépublicaine["mois"] == 12
						// Saute les Sans‐culottides
						? (
							$dateRépublicaine["annéeSextile"]
							? "+6 days"
							: "+5 days"
						)
						// Sinon, ne fait rien de plus
						: "+0 day"
					)
					->format("Y-m-d");

				// L’année précédente commence 
				$dateRépublicaine["annéePrécédente"] = (new DateTime($dateRépublicaine["débutAnnée"]))
					->modify(
						$annéePrécédenteSextile
						? "-" . $f3->get("JOURS_PAR_AN_BISSEXTIL") . " days"
						: "-" . $f3->get("JOURS_PAR_AN") . " days"
					)
					->format("Y-m-d");

				// Quoi qu’il arrive, l’année suivante commence un jour après la date de fin de l’année courante
				$dateRépublicaine["annéeSuivante"] = (new DateTime($dateRépublicaine["finAnnée"]))
					->modify("+1 day")
					->format("Y-m-d");

				/****************************************************************/
				/**************** Rappel des paramètres d’entrée ****************/
				/****************************************************************/

				$dateRépublicaine["paramètres"] = [
					"date" => $date,
					"heure" => $heure,
					"calendrier" => $calendrier,
					"orthographe" => $orthographe,
					"fuseauHoraire" => $fuseauHoraire,
				];

				$dateRépublicaine["données"] = $données;

				return $dateRépublicaine;
			}
		}
	}

?>