<?php

	class CSRF {
		public static function créerJeton (int $duréeLimiteJeton) : void {
			if (!(isset($_SESSION["jetonCSRF"]) && isset($_SESSION["tempsJetonCSRF"])) || (time() - $_SESSION["tempsJetonCSRF"]) > $duréeLimiteJeton) {
				$_SESSION["jetonCSRF"] = Aléatoire::nonce();
				$_SESSION["tempsJetonCSRF"] = time();
			}
		}

		public static function validerJeton (string $jetonReçu) : bool {
			return $_SESSION["jetonCSRF"] == $jetonReçu;
		}
	}

?>