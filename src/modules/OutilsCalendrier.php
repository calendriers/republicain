<?php

	class OutilsCalendrier {
		// Récupère une date selon un format parmi :
		//		– (J)J/(M)M/(A)AAA
		//		– (J)J-(M)M-(A)AAA
		//		– (A)AAA-(M)M-(J)J
		//		– AAAA-JJJ
		// Et les convertit tous vers le format canonique AAAA-MM-JJ
		public static function canoniserDate (string $date) : string {
			// Convertit le format (J)J/(M)M/(A)AAA au format (J)J-(M)M-(AA)AA
			if (preg_match("/^\d{1,2}\/\d{1,2}\/\d{3,4}$/", $date))
				$date = str_replace("/", "-", $date);

			// Convertit le format (J)J-(M)M-(A)AAA au format AAAA-MM-JJ
			if (preg_match("/^\d{1,2}-\d{1,2}-\d{3,4}$/", $date))
				$date = sprintf("%04d-%02d-%02d", ...array_reverse(explode("-", $date)));

			// Convertit le format (A)AAA-(M)M-(J)J au format AAAA-MM-JJ
			if (preg_match("/^\d{3,4}-\d{1,2}-\d{1,2}$/", $date))
				$date = sprintf("%04d-%02d-%02d", ...explode("-", $date));

			// Convertit le format AAAA-JJJ au format AAAA-MM-JJ
			if (preg_match("/^\d{4}-\d{3}$/", $date)) {
				// Récupère l’année et le jour dans l’année 
				[ $année, $jourAnnée, ] = explode("-", $date);

				// Et décrémente le jour car le constructeur de DateTime prend le jour dans l’année entre 0 et 364 ou 365 (ans bissextils)
				$jourAnnée = intval($jourAnnée) - 1;

				// Construit un objet date
				$objetDate = DateTimeImmutable::createFromFormat("Y-z", "$année-$jourAnnée");

				// Et le convertit avant de le retourner
				if ($jourAnnée <= 364 + intval($objetDate->format("L")))
					$date = $objetDate->format("Y-m-d");

				else
					$date = "error";
			}

			return $date;
		}

		public static function canoniserHeureRépublicaine (?string $heure) : string {
			if ($heure == null)
				return "0:00:00.00";

			else if (preg_match("/^[0-9]:[0-9]{1,2}$/", $heure))
				return sprintf("%d:%02d", ...explode(":", $heure)) . ":00.00";

			else if (preg_match("/^[0-9]:[0-9]{1,2}:[0-9]{1,2}$/", $heure))
				return sprintf("%d:%02d:%02d", ...explode(":", $heure)) . ".00";

			else if (preg_match("/^[0-9]:[0-9]{1,2}:[0-9]{1,2}.[0-9]{1,2}$/", $heure)) {
				$parties = explode(".", $heure);
				return sprintf("%d:%02d:%02d", ...explode(":", $parties[0])) . "." . sprintf("%02d", $parties[1]);
			}
		}

		public static function vérifierDate (int $année, int $mois, int $jour) : bool {
			return checkdate($mois, $jour, $année);
		}

		// Permet de savoir si deux nombres sont divisibles ou non
		// Intdiv est utilisée puisque le modulo PHP ne retourne pas de flottant et ne permet donc la comparaison
		public static function estDivisible ($dividende, $diviseur) : bool {
			return $dividende / $diviseur == intdiv($dividende, $diviseur);
		}

		public static function tempsUNIX (string $formatDateHeure) : ?int {
			// Si le format ne contient pas l’heure, ajoute une valeur par défaut
			if (preg_match("/^\d{4}-\d{2}-\d{2}$/", $formatDateHeure))
				$formatDateHeure .= " 00:00:00.000";

			// Si la date et l’heure correspondent au format attendu (AAAA-MM-JJ HH:MM:SS.mmm), retourne le temps UNIX
			if (preg_match("/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}.\d{3}$/", $formatDateHeure))
				return intval((DateTimeImmutable::createFromFormat("Y-m-d H:i:s.v", $formatDateHeure))->format("Uv"));

			// Sinon, retourne une valeur d’erreur
			else
				return null;
		}
	}

?>