<?php

	class CalendrierGrégorien {
		// Indique si une année, grégorienne ou républicaine (selon la variante révisée), est bissextile
		public static function estBissextile (int $année) : bool {
			// Si l’année est négative, la ramène à une valeur appropriée au calcul en l’incrémentant d’un car l’année civile 0 n’existe pas
			if ($année < 0)
				$année = $année + 1;

			// L’année est multiple de 4 sans être multiple de 100, à moins qu’elle soit aussi multiple de 400 sans être multiple de 4 000
			return OutilsCalendrier::estDivisible($année, 4) && (!OutilsCalendrier::estDivisible($année, 100) || (OutilsCalendrier::estDivisible($année, 400) && !OutilsCalendrier::estDivisible($année, 4_000)));
		}

		public static function obtenirTemps (?string $date, ?string $heure, string $fuseauHoraire) : ?int {
			$f3 = \Base::instance();

			// Définit le fuseau horaire à partir des paramètres
			date_default_timezone_set($fuseauHoraire);

			// Si l’utilisateur n’a défini ni date ni heure, utilise la date et l’heure du serveur
			if ($date == null && $heure == null)
				// Puis obtient le temps UNIX courant en millisecondes écoulées depuis Epoch
				// La précision de microtime dépend de la précision des flottants en PHP (php.ini)
				$tempsGrégorien = intval(microtime(true) * $f3->get("MILLISECONDES_PAR_SECONDE"));

			// Sinon, utilise les informations fournies et calcule le nombre de secondes écoulées depuis Epoch
			else {
				// Si la date est définie
				if ($date != null) {
					// Récupère son format canonique
					$date = OutilsCalendrier::canoniserDate($date);

					// Enfin, si la date ne correspond pas au format canonique attendu (AAAA-MM-JJ) ou si elle n’existe pas
					// Interrompt la demande avec un code d’erreur
					if (!preg_match("/^\d{4}-\d{2}-\d{2}$/", $date) || !OutilsCalendrier::vérifierDate(...explode("-", $date)))
						return null;
				}

				// Si la date n’est pas définie, utilise celle du serveur
				else
					$date = (new DateTimeImmutable())->format("Y-m-d");

				// Si l’heure est définie
				if ($heure != null) {
					// Si l’heure ne correspond pas au format canonique attendu (HH:MM[:SS[.mmm]])
					// Interrompt la demande avec un code d’erreur
					if (!preg_match("/^(?:[01][0-9]|2[0-3]):[0-5][0-9](?::[0-5][0-9](?:.[0-9]{3})?)?$/", $heure))
						return null;

					// Si l’heure est au format HH:MM, ajoute les secondes
					if (strlen($heure) == 5)
						$heure .= ":00";

					// Si l’heure est au format HH:MM:SS, ajoute les millisecondes
					if (strlen($heure) == 8)
						$heure .= ".000";
				}

				// Si l’heure n’est pas définie, utilise une valeur par défaut
				else
					$heure = "00:00:00.000";

				// Puis convertit enfin en temps UNIX la date et l’heure courantes
				$tempsGrégorien = OutilsCalendrier::tempsUNIX("$date $heure");
			}

			return $tempsGrégorien;
		}

		public static function calculerDate (?string $jour, ?string $mois, ?string $an, ?string $horaire, string $calendrier, string $orthographe, string $fuseauHoraire) : ?array {
			$f3 = \Base::instance();

			// Charge les données
			$données = JSON::chargerDonnées($orthographe);

			// Si…
			if (
				// Un des paramètres reçus est indéfini ou mal formaté
				$jour == null || $mois == null || $an == null ||
				!preg_match("/^\d{1,2}$/", $jour) ||
				!preg_match("/^\d{1,2}$/", $mois) ||
				!preg_match("/^-?\d{1,4}$/", $an) ||
				($horaire != null && !preg_match("/^[0-9]:[0-9]{1,2}(?::[0-9]{1,2}(?:.[0-9]{1,2})?)?$/", $horaire)) ||
				!in_array($calendrier, $f3->get("CALENDRIERS")) ||
				!in_array($orthographe, $f3->get("ORTHOGRAPHES")) ||
				!in_array($fuseauHoraire, $f3->get("FUSEAUX_HORAIRES")) ||

				// Ou que la date est invalide car…
				// Le jour est incorrect
				($jour > $f3->get("JOURS_PAR_MOIS_RÉPUBLICAIN") || $jour < 1) ||
				// Le mois est incorrect
				($mois > ($f3->get("MOIS_PAR_AN") + 1) || $mois < 1) ||
				// Le jour est incorrect dans la semi‐décade des sans‐culottides
				($mois == ($f3->get("MOIS_PAR_AN") + 1) && $jour > 6)
			) {
				// Interrompt la demande
				return null;
			}

			else {
				// Récupère l’année et la position du jour dans l’année
				$annéeRépublicaine = intval($an);
				$jourRépublicain = intval($jour) + ((intval($mois) - 1) * $f3->get("JOURS_PAR_MOIS_RÉPUBLICAIN"));
				// Ainsi que le mode de calcul
				$modeRévisé = ($calendrier == $f3->get("CALENDRIER_RÉVISÉ"));

				// Estime la date grégorienne de commencement de l’année républicaine donnée
				// Ne soustrait un à l’estimation que pour les années positives et non négatives (proleptiques)
				$annéeGrégorienneSupposée = $annéeRépublicaine + $f3->get("ANNÉE_COMMENCEMENT") + ($annéeRépublicaine >= 1 ? -1 : 0);
				$jourCommencement = CalendrierRépublicain::estimerJourCommencementAnnée($annéeGrégorienneSupposée, $modeRévisé);

				// Initialise les années grégorienne et républicaine s’équivalant
				$annéeRépublicaineCourante = $annéeRépublicaine;
				$annéeGrégorienneCourante = $annéeGrégorienneSupposée;

				// Puis les compteurs de jours
				$jourRépublicainCourant = 1;
				$jourGrégorienCourant = $jourCommencement;

				// Tant que le jour et l’année ne concordent pas, passe une journée
				while ($jourRépublicainCourant < $jourRépublicain || $annéeRépublicaineCourante < $annéeRépublicaine) {
					$jourRépublicainCourant++;
					$jourGrégorienCourant++;

					// Si le jour de l’année déborde selon que l’année est bissextile ou non, incrémente l’année et remet le jour à 1
					if (
						(CalendrierGrégorien::estBissextile($annéeGrégorienneCourante) && $jourGrégorienCourant > $f3->get("JOURS_PAR_AN_BISSEXTIL")) ||
						(!CalendrierGrégorien::estBissextile($annéeGrégorienneCourante) && $jourGrégorienCourant > $f3->get("JOURS_PAR_AN"))
					)  {
						$annéeGrégorienneCourante += 1;
						$jourGrégorienCourant = 1;
					}
				}

				// Interrompt la demande si le jour demandé est le sixième sans‐culottide d’une année non sextile
				if ($jour == 6 && $mois == 13 && !CalendrierRépublicain::estSextile($annéeRépublicaineCourante, $modeRévisé))
					return null;

				// Ou si l’année grégorienne est inférieure à l’an 1 ou supérieure à l’an 9999
				if ($annéeGrégorienneCourante < 1 || $annéeGrégorienneCourante > 9999)
					return null;

				// Objet décrivant les caractéristiques de la date républicaine
				$date = (new DateTime(sprintf("%04d", $annéeGrégorienneCourante) . "-1-1"))
					->modify("+" . ($jourGrégorienCourant - 1) . " days");

				$horaire = OutilsCalendrier::canoniserHeureRépublicaine($horaire);
				preg_match("/^(\d):(\d{2}):(\d{2}).(\d{2})$/", $horaire, $partiesHoraireRépublicain, PREG_UNMATCHED_AS_NULL);

				$heuresRépublicaines = intval($partiesHoraireRépublicain[1]);
				$minutesRépublicaines = intval($partiesHoraireRépublicain[2]);
				$secondesRépublicaines = intval($partiesHoraireRépublicain[3]);
				$centisecondesRépublicaines = intval($partiesHoraireRépublicain[4]);

				$centisecondesRépublicainesJournée = $heuresRépublicaines * $f3->get("CENTISECONDES_PAR_HEURE_RÉPUBLICAINE") + $minutesRépublicaines * $f3->get("CENTISECONDES_PAR_MINUTE_RÉPUBLICAINE") + $secondesRépublicaines * $f3->get("CENTISECONDES_PAR_SECONDE_RÉPUBLICAINE") + $centisecondesRépublicaines;

				// Produit en croix pour obtenir le nombre de millisecondes grégoriennes écoulées
				// On omet le facteur 1 000 des millisecondes puisqu’il est commun au numérateur et dénominateur
				// Mais on veut en plus ramener ce nombre à des millisecondes donc on multiplie le numérateur par 10
				$millisecondesGrégoriennesJournée = round(($centisecondesRépublicainesJournée * $f3->get("SECONDES_PAR_JOUR") * 10) / $f3->get("SECONDES_PAR_JOUR_RÉPUBLICAIN"));

				$secondesGrégoriennesJournée = intdiv($millisecondesGrégoriennesJournée, $f3->get("MILLISECONDES_PAR_SECONDE"));
				$heures = intdiv($secondesGrégoriennesJournée, $f3->get("SECONDES_PAR_HEURE"));
				$minutes = intdiv($secondesGrégoriennesJournée % $f3->get("SECONDES_PAR_HEURE"), $f3->get("SECONDES_PAR_MINUTE"));
				$secondes = ($secondesGrégoriennesJournée % $f3->get("SECONDES_PAR_HEURE")) % $f3->get("SECONDES_PAR_MINUTE");
				$millisecondes = $millisecondesGrégoriennesJournée % $f3->get("MILLISECONDES_PAR_SECONDE");

				// Remonte les minutes en conséquence
				if ($secondes == $f3->get("SECONDES_PAR_MINUTE")) {
					$secondes = 0;
					$minutes++;

					// Et les heures de même
					if ($minutes == $f3->get("MINUTES_PAR_HEURE")) {
						$minutes = 0;
						$heures++;
					}
				}

				$horaireGrégorien = sprintf("%02d", $heures) . ":" . sprintf("%02d", $minutes) . ":" . sprintf("%02d", $secondes) . "." . sprintf("%03d", $millisecondes);

				$retour = CalendrierGrégorien::obtenirDate(CalendrierGrégorien::obtenirTemps($date->format("Y-m-d"), $horaireGrégorien, $fuseauHoraire), $données, true);
				$retour["données"] = $données;
				$retour["paramètres"] = [
					"jour" => intval($jour),
					"mois" => intval($mois),
					"an" => intval($an),
					"heures" => $heuresRépublicaines,
					"minutes" => $minutesRépublicaines,
					"secondes" => $secondesRépublicaines,
					"centisecondes" => $centisecondesRépublicaines,
				];

				return $retour;
			}
		}

		public static function obtenirDate (?int $tempsGrégorien, array $données, bool $heure = true) : ?array {
			$f3 = \Base::instance();

			if ($tempsGrégorien == null)
				return null;

			// Isole les millisecondes du temps UNIX
			$millisecondes = $tempsGrégorien % $f3->get("MILLISECONDES_PAR_SECONDE");

			// Puis récupère un objet décrivant la date [fonction getdate] du temps UNIX courant (grégorien)
			$objetDateGrégorienne = getdate(intdiv($tempsGrégorien, $f3->get("MILLISECONDES_PAR_SECONDE")));

			// Récupère le jour grégorien depuis l’objet de date (dimanche valant 0, il est remplacé par 7)
			$jourGrégorien = $objetDateGrégorienne["wday"] == 0 ? 7 : $objetDateGrégorienne["wday"];

			// Convertit cette date grégorienne dans un format plus agréable à exploiter
			$dateGrégorienne = [];

			// Rappelle le temps UNIX courant en millisecondes et en secondes
			$dateGrégorienne["tempsUNIXms"] = $tempsGrégorien;
			$dateGrégorienne["tempsUNIX"] = intdiv($tempsGrégorien, $f3->get("MILLISECONDES_PAR_SECONDE"));

			if ($heure) {
				$dateGrégorienne["millisecondes"] = $millisecondes;
				$dateGrégorienne["secondes"] = $objetDateGrégorienne["seconds"];
				$dateGrégorienne["minutes"] = $objetDateGrégorienne["minutes"];
				$dateGrégorienne["heures"] = $objetDateGrégorienne["hours"];
				$dateGrégorienne["secondesJournée"] = $dateGrégorienne["heures"] * $f3->get("SECONDES_PAR_HEURE") + $dateGrégorienne["minutes"] * $f3->get("SECONDES_PAR_MINUTE") + $dateGrégorienne["secondes"];
				$dateGrégorienne["millisecondesJournée"] = $dateGrégorienne["secondesJournée"] * $f3->get("MILLISECONDES_PAR_SECONDE") + $dateGrégorienne["millisecondes"];
			}

			$dateGrégorienne["jourSemaine"] = $jourGrégorien;
			$dateGrégorienne["jourMois"] = $objetDateGrégorienne["mday"];
			$dateGrégorienne["jourAnnée"] = $objetDateGrégorienne["yday"] + 1;
			$dateGrégorienne["nomJourSemaine"] = $données["nomsJoursSemaineGrégorienne"][$jourGrégorien - 1];

			$dateGrégorienne["mois"] = $objetDateGrégorienne["mon"];
			$dateGrégorienne["nomMois"] = $données["nomsMoisGrégoriens"][$dateGrégorienne["mois"] - 1];

			$dateGrégorienne["année"] = $objetDateGrégorienne["year"];
			$dateGrégorienne["annéeChiffresArabes"] = Format::année($dateGrégorienne["année"], $f3->get("CHIFFRAGE_ARABE"));
			$dateGrégorienne["annéeChiffresRomains"] = Format::année($dateGrégorienne["année"], $f3->get("CHIFFRAGE_ROMAIN"));

			if ($heure) {
				$dateGrégorienne["millisecondesChaine"] = sprintf("%03d", $dateGrégorienne["millisecondes"]);
				$dateGrégorienne["secondesChaine"] = sprintf("%02d", $dateGrégorienne["secondes"]);
				$dateGrégorienne["minutesChaine"] = sprintf("%02d", $dateGrégorienne["minutes"]);
				$dateGrégorienne["heuresChaine"] = sprintf("%02d", $dateGrégorienne["heures"]);
			}

			$dateGrégorienne["jourChaine"] = sprintf("%02d", $dateGrégorienne["jourMois"]);
			$dateGrégorienne["moisChaine"] = sprintf("%02d", $dateGrégorienne["mois"]);
			$dateGrégorienne["annéeChaine"] = sprintf("%04d", $dateGrégorienne["année"]);

			$tableauJourMoisAnnée = [ $dateGrégorienne["jourChaine"], $dateGrégorienne["moisChaine"], $dateGrégorienne["annéeChaine"], ];
			$tableauAnnéeMoisJour = array_reverse($tableauJourMoisAnnée);

			$dateGrégorienne["dateChaine"] = implode("/", $tableauJourMoisAnnée);
			$dateGrégorienne["dateChaineInversée"] = implode("/", $tableauAnnéeMoisJour);

			$dateGrégorienne["dateChaineTirets"] = implode("-", $tableauJourMoisAnnée);
			$dateGrégorienne["dateChaineTiretsInversée"] = implode("-", $tableauAnnéeMoisJour);

			if ($heure)
				$dateGrégorienne["heureChaine"] = $dateGrégorienne["heuresChaine"] . ":" . $dateGrégorienne["minutesChaine"];

			return $dateGrégorienne;
		}
	}

?>