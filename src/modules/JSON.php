<?php

	class JSON {
		public static function lire (string $adresseFichier) : array {
			return json_decode(file_get_contents($adresseFichier), true);
		}

		public static function chargerDonnées (string $orthographe) : array {
			// Chargement du fichier de données
			return JSON::lire(__DIR__ . "/../ressources/données/$orthographe.json");
		}
	}

?>