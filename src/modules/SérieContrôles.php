<?php

	class SérieContrôles {
		public static function lancer (string $fichier, string $nomDateEntrée, string $nomDateSortie, string $nomFormatRéponseSpécifique, callable $fonctionConversionDate) : array {
			$contrôles = JSON::lire($fichier);

			foreach ($contrôles as &$listeContrôle) {
				$erreurs = 0;

				foreach ($listeContrôle["liste"] as &$contrôle) {
					// Si la date d’entrée est indéfinie, il s’agit d’un contrôle de date incorrecte en sens inverse donc passe sans erreur
					if ($contrôle[$nomDateEntrée] == null) {
						$contrôle["résultat"] = "‹date indéfinie›";
						$contrôle["échoué"] = false;
					}

					else {
						$dateEntrée = array_merge($listeContrôle[$nomDateEntrée], $contrôle[$nomDateEntrée]);
						$dateSortieAttendue = array_merge($listeContrôle[$nomDateSortie], $contrôle[$nomDateSortie] ?? []);

						// Calcule la date de sortie à partir des données configurées dans le contrôle ou la liste de contrôle
						$dateSortieCalculée = $fonctionConversionDate($dateEntrée, $contrôle, $listeContrôle);

						$contrôle["attendu"] = ($contrôle[$nomDateSortie] !== null)
							// Si la date de sortie est strictement non‐nulle, formate la date attendue
							? Format::chaineDepuisDate($dateSortieAttendue, $listeContrôle[$nomFormatRéponseSpécifique] ?? $listeContrôle["formatRéponse"], $listeContrôle["délimiteurFormat"])
							// Sinon, la définit comme étant une date incorrecte
							: "‹date incorrecte›";

						$contrôle["résultat"] = ($dateSortieCalculée !== null)
							// Si la date calculée est strictement non‐nulle, formate la date de résultat
							? Format::chaineDepuisDate($dateSortieCalculée, $listeContrôle[$nomFormatRéponseSpécifique] ?? $listeContrôle["formatRéponse"], $listeContrôle["délimiteurFormat"])
							// Sinon, la définit comme étant une date incorrecte
							: "‹date incorrecte›";

						// Vérifie si le contrôle a échoué…
						$contrôle["échoué"] = $contrôle["résultat"] !== $contrôle["attendu"];

						// Et incrémente le compteur d’erreurs dans ce cas
						if ($contrôle["échoué"])
							$erreurs++;
					}
				}

				$listeContrôle["erreurs"] = $erreurs;
			}

			return $contrôles;
		}
	}

?>