<?php

	class Format {
		public static function préformaté ($variable) : string {
			return "<pre>" . print_r($variable, 1) . "</pre>";
		}

		public static function adresseComplèteImage (array $données, $nomImage) : string {
			$f3 = \Base::instance();

			return $f3->get("HOST") . Format::adresseRelativeImage($données, $nomImage);
		}

		public static function adresseRelativeImage (array $données, $nomImage) : string {
			$f3 = \Base::instance();

			return $f3->get("BASE") . "/" . $données["dossierImagesMois"] . "/" . $nomImage . "." . $données["formatImages"];
		}

		public static function nomJour (array $jour, array $préfixes) : string {
			if ($jour["pluriel"])
				$préfixe = $préfixes["pluriel"];

			else if ($jour["liaison"])
				$préfixe = $préfixes["liaison"];

			else
				$préfixe = $jour["féminin"] ? $préfixes["féminin"] : $préfixes["masculin"];

			return $préfixe . $jour["nom"];
		}

		public static function jourMois (int $numéroMois) : string {
			return ($numéroMois === 1) ? $numéroMois . "<sup>er</sup>" : $numéroMois;
		}

		public static function ordinal (int $numéro, bool $féminin = false, bool $pluriel = false) : string {
			$f3 = \Base::instance();

			if ($f3->get("PARAMÈTRES")["ordination"] == $f3->get("ORDINATION_TRADITIONNELLE"))
				return $numéro . "<sup>" . (($numéro == 1) ? ($féminin ? "re" : "er") : "e") . ($pluriel ? "s": "") . "</sup>";

			else
				return $numéro . "<sup>" . (($numéro == 1) ? ($féminin ? "ère" : "er") : "ème") . ($pluriel ? "s": "") . "</sup>";
		}

		public static function année (int $année, string $mode) : string {
			return ($année >= 0 ? "" : "−") . ($mode == "romain" ? Format::chiffresRomains(abs($année)) : abs($année));
		}

		public static function date (string $date) : string {
			return implode("/", array_reverse(explode("-", $date)));
		}

		public static function chaineDepuisDate (array $date, string $format, string $délimiteurFormat) : string {
			return implode($délimiteurFormat, array_map(function ($nomPropriété) use ($date) {
				return intval($date[$nomPropriété]);
			}, explode($délimiteurFormat, $format)));
		}

		public static function fuseauUTC (float $fuseau) : string {
			return "UTC" . ($fuseau >= 0 ? "+" : "−") . floor(abs($fuseau)) . ":" . sprintf("%02d", (abs(fmod($fuseau, 1)) * 60));
		}

		public static function saisonHoraire (int $tempsUNIX) : string {
			return date("I", $tempsUNIX) ? "été" : "hiver";
		}

		public static function chiffresRomains (int $nombre) : string {
			// Maximum : 399 999
			$chiffresRomains = "";
			$chaineNombre = strrev("$nombre");
			$équivalences = [ [ "I", "V", ], [ "X", "L", ], [ "C", "D" ], [ "M", "ↁ", ], [ "ↂ", "ↇ", ], [ "ↈ", ], ];

			for ($i = 0; $i < strlen($chaineNombre); $i++) {
				// Les zéros sont ignorés
				if ($chaineNombre[$i] == "0")
					continue;

				// Les valeurs inférieurs ou égales à 3 sont simplement des duplications (III, XXX…)
				else if ($chaineNombre[$i] <= 3) {
					$compte = $chaineNombre[$i];

					while ($compte-- > 0)
						$chiffresRomains .= $équivalences[$i][0];
				}

				// Les valeurs égales à 4 sont particulière (IV, XL…)
				else if ($chaineNombre[$i] == 4)
					$chiffresRomains .= $équivalences[$i][1] . $équivalences[$i][0];

				// Les valeurs de 5 à 8 sont similaires ([VLD][IXC]{0,3}…)
				else if ($chaineNombre[$i] >= 5 && $chaineNombre[$i] <= 8) {
					$valeur = $chaineNombre[$i];

					while ($valeur-- > 5)
						$chiffresRomains .= $équivalences[$i][0];

					$chiffresRomains .= $équivalences[$i][1];
				}

				// Les valeurs égales à 9 sont particulières (IX, XC…)
				else
					$chiffresRomains .= $équivalences[$i + 1][0] . $équivalences[$i][0];
			}

			$retour = null;
			preg_match_all("/./u", $chiffresRomains, $retour);
			return implode("", array_reverse($retour[0]));
		}
	}

?>