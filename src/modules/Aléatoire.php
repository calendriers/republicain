<?php

	class Aléatoire {
		public static function chaineAléatoire (int $taille = 256) : string {
			$nombreOctets = ceil($taille / 8);

			$retour = "";

			while ($nombreOctets--)
				$retour .= chr(mt_rand(0, 255));

			return $retour;
		}

		public static function nonce () : string {
			return hash("sha256", Aléatoire::chaineAléatoire());
		}
	}

?>