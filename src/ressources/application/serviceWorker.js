// Clé de version de l’agent de service
const VERSION = "v1"

// Identifiant du cache de l’application progressive
const IDENTIFIANT_CACHE = `cache-republicain-${VERSION}`

const BASE = `${location.protocol}//${location.host}/republicain`

// Adresse de la page en cas de hors‐ligne
const PAGE_HORS_LIGNE = `${BASE}/ressources/application/horsLigne.html`

const RESSOURCES_CACHE = [
	PAGE_HORS_LIGNE,
	`${BASE}/ressources/style.css`,
	`${BASE}/ressources/style.nuit.css`,
	`${BASE}/ressources/polices/Italiano.woff2`,
	`${BASE}/ressources/polices/Italiano étendue.woff2`,
	`${BASE}/ressources/images/sans‐culottides-min.jpg`,
]

// Procède à l’installation de l’agent de service
self.addEventListener("install", évènement => {
	// Initialise un cache contenant la page en cas de hors‐ligne
	évènement.waitUntil((async () => {
		const cache = await caches.open(IDENTIFIANT_CACHE)
		await cache.addAll(RESSOURCES_CACHE)
	})())

	// Force l’installation de cette nouvelle version
	self.skipWaiting()
})

// Procède à l’activation de l’agent de service
self.addEventListener("activate", évènement => {
	// Active le préchargement des ressources si possible
	évènement.waitUntil((async () => {
		if (self.registration.navigationPreload)
			await self.registration.navigationPreload.enable()
	})())

	// Réclame la priorité de cette nouvelle version sur la page courante
	self.clients.claim()

	// Et invalide les clés du cache correspondant aux anciennes version
	évènement.waitUntil((async () => {
		const clés = await caches.keys()

		await Promise.all(
			clés.map(clé => {
				if (!clé.includes(IDENTIFIANT_CACHE))
					return caches.delete(clé)
			})
		)
	})())
})

// En cas de requête
self.addEventListener("fetch", évènement => {
	// Si la requête concerne une navigation interne
	if ([ "navigate", "cors", ].includes(évènement.request.mode)) {
		// Répond avec un objet Réponse…
		évènement.respondWith((async () => {
			try {
				const préchargement = await évènement.preloadResponse

				// Si la réponse a pu être préchargée, la retourne
				if (préchargement)
					return préchargement

				// Sinon, effectue la requête
				else
					return await fetch(évènement.request)
			}

			// En cas d’erreur sur le réseau
			catch (erreur) {
				// Ouvre le cache et récupère la page en cas de hors‐ligne
				const cache = await caches.open(IDENTIFIANT_CACHE)
				return await cache.match(PAGE_HORS_LIGNE)
			}
		})())
	}

	// Sinon, la requête concerne des ressources (images, polices et scripts)
	else {
		évènement.respondWith((async () => {
			const cache = await caches.open(IDENTIFIANT_CACHE)

			const préchargement = await évènement.preloadResponse

			// Si la réponse a pu être préchargée, la met en cache et la retourne
			if (préchargement) {
				cache.put(évènement.request, ressourceRequête.clone())
				return préchargement
			}

			// Sinon
			else {
				// Cherche la ressource en cache
				const ressourceCache = await cache.match(évènement.request)

				// Si elle y est, la retourne
				if (ressourceCache)
					return ressourceCache

				// Sinon
				else {
					// Effectue la requête
					const ressourceRequête = await fetch(évènement.request)

					// Et la met en cache
					cache.put(évènement.request.url, ressourceRequête.clone())

					// Puis retourne
					return ressourceRequête
				}
			}
		})())
	}
})
