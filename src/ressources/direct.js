// – une heure républicaine dure 2 heures et 24 minutes grégoriennes
// – une minute républicaine (100e d’heure) dure donc 1 minute et 26 secondes grégoriennes
// – une seconde républicaine (100e de minute) dure donc 864 millisecondes grégoriennes
// – une centiseconde républicaine (100e de seconde) dure donc 8,64 millisecondes grégoriennes
const DURÉE_CENTISECONDE_RÉPUBLICAINE = 8.64 // ms

const convertirNombre = (n) => parseInt(n, 10)

function parseBool (chaine) {
	if (chaine === "true")
		return true

	else if (chaine === "false")
		return false

	else
		throw new Error("La valeur booléenne fournise n’est pas correcte.")
}

// Mettre à jour les informations d’horaire sur la page en continu
function incrémenterObjetSystèmeHoraire (compteur, rafraichissement, incrément) {
	let valeurCourante = compteur.valeur,
		valeurNouvelle = (compteur.valeur + incrément) % compteur.seuil,
		débordement = Math.floor((compteur.valeur + incrément) / compteur.seuil)

	compteur.valeur = valeurNouvelle

	if (compteur.rafraichissement)
		compteur.balise.innerText = valeurNouvelle.toString().padStart(Math.log10(compteur.seuil), "0")

	// Et incrémente le compteur parent seulement si la valeur vient d’atteindre son seuil
	if (débordement > 0) {
		// Et que le parent existe
		if (compteur.parent !== null)
			return incrémenterObjetSystèmeHoraire(compteur.parent, rafraichissement, débordement)

		// Sinon, recharge la page pour obtenir la nouvelle journée…
		else {
			// Via Swup si le rafraichissement est activé…
			if (rafraichissement) {
				if (window.instanceSwup) {
					déchargerComposants()

					instanceSwup.loadPage({
						"url": location.pathname,
						"method": "GET",
					})

					chargerComposants()
				}

				// Ou complètement sinon
				else
					location.reload()
			}

			// Autrement, laisse les compteurs boucler sur eux‐mêmes
		}
	}
}

function chargerComposants () {
	const contexte = document.querySelector("[data-ajourneur]")

	if (contexte) {
		const ajournement = parseBool(contexte.getAttribute("data-ajourneur"))
		const rafraichissement = parseBool(contexte.getAttribute("data-rafraichissement"))

		if (ajournement) {
			// Éléments à mettre à jour
			const indicateurHeures = document.querySelector("#heures")
			const indicateurMinutes = document.querySelector("#minutes")
			const indicateurSecondes = document.querySelector("#secondes")
			const indicateurCentisecondes = document.querySelector("#centisecondes")

			window.indicateurs = {}

			indicateurs.heures = {
				parent: null,
				balise: indicateurHeures,
				valeur: convertirNombre(indicateurHeures.innerText),
				seuil: 10,
				rafraichissement: true,
			}

			indicateurs.minutes = {
				parent: indicateurs.heures,
				balise: indicateurMinutes,
				valeur: convertirNombre(indicateurMinutes.innerText),
				seuil: 100,
				rafraichissement: true,
			}

			indicateurs.secondes = {
				parent: indicateurs.minutes,
				balise: indicateurSecondes,
				valeur: convertirNombre(indicateurSecondes.innerText),
				seuil: 100,
				rafraichissement: true,
			}

			indicateurs.centisecondes = {
				parent: indicateurs.secondes,
				balise: indicateurCentisecondes,
				valeur: convertirNombre(indicateurCentisecondes.innerText),
				seuil: 100,
				rafraichissement: false,
			}

			// Calcule le temps en microsecondes au chargement, puis sera mis à jour à chaque actualisation
			let dernierTemps = (new Date()).getTime()

			// Les centisecondes sont actualisées, les autres valeurs se changeront en chaine
			window.rafraichirTemps = function () {
				let nouveauDernierTemps = (new Date()).getTime()
				let écart = nouveauDernierTemps - dernierTemps

				// Si l’écart entre le nouveau temps et le dernier temps est supérieure à la durée d’une centiseconde
				if (écart >= DURÉE_CENTISECONDE_RÉPUBLICAINE) {
					// On actualise le compteur
					incrémenterObjetSystèmeHoraire(indicateurs.centisecondes, rafraichissement, Math.floor(écart / DURÉE_CENTISECONDE_RÉPUBLICAINE))

					// On met à jour le dernier temps pour qu’il prenne la valeur du dernier temps moins l’imprécision restante pour la reporter sur le prochain calcul et être sûr qu’elle sera prise en compte
					dernierTemps = nouveauDernierTemps - écart % DURÉE_CENTISECONDE_RÉPUBLICAINE
				}

				window.intervalleRafraichissementTemps = requestAnimationFrame(rafraichirTemps)
			}

			window.intervalleRafraichissementTemps = requestAnimationFrame(rafraichirTemps)
		}
	}
}

function déchargerComposants () {
	cancelAnimationFrame(window.intervalleRafraichissementTemps)
	delete window.intervalleRafraichissementTemps
}

window.addEventListener("DOMContentLoaded", function () {
	if ("serviceWorker" in navigator) {
		navigator.serviceWorker.register("/republicain/serviceWorker.js", {
			"scope": "/republicain/",
		})
		.catch(erreur => console.error(erreur))
	}
})
