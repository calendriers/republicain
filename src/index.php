<?php

	$f3 = require("lib/base.php");
	$f3->config("configuration.ini");

	$f3->set("ONERROR", function ($f3) {
		// Nettoie les sorties déjà générées
		while (ob_get_level())
			ob_end_clean();

		// Et renvoie vers le gestionnaire d’erreur de l’application
		Républicain::erreur(500);
	});

	if ($f3->get("DEBUG") > 0) {
		ini_set("display_errors", 1);
		error_reporting(E_ALL);
	}

	else {
		ini_set("display_errors", 0);
		error_reporting(0);
	}

	require("constantes.php");

	$f3->run();

?>